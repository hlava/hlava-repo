<?php include('layout/include_items.php');

include('config/db_majo.php');
//number: number, from: from, to: to, projectype: projectype, coordinator: coordinator,
// partners: partners, web: web, internalcode: internalcode, annotationSK: annotationSK

$id = $_POST['id'];

$sql = "SELECT * FROM projekty WHERE id =".$id.";";
$result = $conn->query($sql);
if (($result->num_rows) > 0) {
    $row = $result->fetch_assoc();


    if ($lang == "sk") {
        $name = $row['titleSK'];
        $annotation = $row['annotationSK'];
    }
    else{
        $name = $row['titleEN'];
        $annotation = $row['annotationEN'];
    }

    $number = $row['number'];
    $coordinator = $row['coordinator'];
    $duration= $row['duration'];
    $from = $row['from'];
    $to = $row['to'];
    $projectType = $row['projectType'];
    $partners = $row['partners'];
    $web = $row['web'];
    $internalCode = $row['internalCode'];

    echo "<table class='table table-bordered'><thead>";
    echo "<tr ><th class='bg-primary'colspan='2' style='text-align: center'> $name </th>";
    echo "</thead><tbody>";
    ?>
    <tr><td class='bg-info'><?php text(typ_projekt)?></td><td ><?php echo $projectType ?></td></tr>
    <tr><td class='bg-info'><?php text(cislo_projekt)?></td><td ><?php  echo $number ?></td></tr>
    <tr><td class='bg-info'><?php text(doba_trvania)?></td><td ><?php  echo $duration ?></td></tr>
    <tr><td class='bg-info'><?php text(coordinator)?></td><td ><?php  echo $coordinator ?></td></tr>
    <?php if ($partners != NULL) echo "<tr><td class='bg-info'>".text_without_echo(partners)."</td><td >".$partners."</td></tr>";
    if ($web!= NULL) echo "<tr><td class='bg-info'>".text_without_echo(web)."</td><td >".$web."</td></tr>";
    if ($internalCode != NULL) echo "<tr><td class='bg-info'>".text_without_echo(internal_code)."</td><td >".$internalCode."</td></tr>";
    if ($annotation != NULL) echo "<tr><td class='bg-info'>".text_without_echo(anotacia)."</td><td >".$annotation."</td></tr>";

    echo "</tbody></table>";


}