<?php
/**
 * Created by PhpStorm.
 * User: Tavarez
 * Date: 2.4.2017
 * Time: 23:33
 */
session_start();
$lang = $_SESSION["lang"];
session_unset();
$_SESSION["lang"] = $lang;
header('Location: index.php');