<?php

include('layout/head.php');

$query = "SELECT * FROM media order by date;";
$sql = $db->prepare($query);
$sql->execute();
$result = $sql->fetchAll();
$count = $sql->rowCount();

echo "<div class='center-block'>";
echo "<h2>"; 
text("mediaZoznamClankov");
echo "</h2>";

for ($i = 0; $i < $count; $i++) {
    
    if($result[$i]->pdf == null){
       echo "<h4><a target='_blank' href='".$result[$i]->url."' style='text-align:center'>" . $result[$i]->title . "</a></h4>";
       echo "<p>";
       text("mediaMedium");
       echo ": ".$result[$i]->media."</p>";
       
       echo "<p>";
       text("mediaDatum");
       echo ": " .$result[$i]->date."</p>";
    }
    else{
       echo "<h4><a target='_blank' href='files/".$result[$i]->pdf."' style='text-align:center'>" . $result[$i]->title . "</a></h4>";
       echo "<p>";
       text("mediaMedium");
       echo ": ".$result[$i]->media."</p>";
       
       echo "<p>";
       text("mediaDatum");
       echo ": " .$result[$i]->date."</p>";
    }
}
echo "</div>";
include('layout/foot.php');
?>
