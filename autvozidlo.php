<?php
include('layout/head.php');
?>
    <h2>
        Autonómne vozidlo 6×6
    </h2><br>
    <img src="files/dve_vozidla.png" alt="Autonómne vozidlo 6×6" style="width: 100%"><br>

    <div style="width: 50%; float: left"">
        <h2 style="font-size: x-large">Technické údaje</h2>
        <li>Hmotnosť: 12,5kg</li>
        <li>Rozmery (d x š x v): 614 x 495 x 269 mm</li>
        <li>Spôsob ovládania: Diaľkové ovládanie, riadené mikroprocesorom</li>
        <li>Pohon: 6×6, každé koleso samostatne riadené</li>
        <li>BLDC elektromotorom</li>
        <li>Celkový výkon elek­tro­mo­torov: 6x 175W</li>
        <li>Napájanie motorov: 6x DC/​AC menič</li>
        <li>Zdroj el. prúdu: 4x Li-​Pol akumulátory</li>
        <li>Celková kapacita aku­mulá­torov: 13,2 Ah</li>
    </div>

    <div style="width: 50%; float: right">
        <img src="files/Render_ISO.jpg" alt="Autonómne vozidlo 6×6" style="width: 100%"><br>
    </div>



<?php include('layout/foot.php'); ?>