<?php
include('layout/head.php');
redirectIfNotLogged($user); 
$selected_month = isset($_GET["mesiac"]) ? $_GET["mesiac"] : 2;
$selected_year = isset($_GET["rok"]) ? $_GET["rok"] : 2017;
$days_in_month = cal_days_in_month(CAL_GREGORIAN, $selected_month, $selected_year);
if($lang == "sk"){
$mesiac = ["", "Január", "Február", "Marec", "Apríl", "Máj", "Jún", "Júl", "August", "September", "Október", "November", "December"];
}else{
$mesiac =  ["",'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
}
$from = 2017; 
$to = 2050;   
 
?>
    <h2>
        <i class="fa fa-child" aria-hidden="true"></i> <?php text('dochadzka');?> <i class="fa fa-child" aria-hidden="true"></i>
    </h2>
    <form>
        <div class="form-group">  
            <div class="form-inline">
                <label for="mesiac"><?php text('mesiac');?> :</label>
                <select class="form-control" id="mesiac" name="mesiac">
                    <?php
                    for ($tmp = 1; $tmp < 13; $tmp++) {
                        if ($selected_month == $tmp) {
                            echo "<option value='$tmp' selected>$mesiac[$tmp]</option>";
                        } else {
                            echo "<option value='$tmp'>$mesiac[$tmp]</option>";
                        }
                    }
                    ?>
                </select>
                <label for="rok"><?php text('rok');?> :</label>
                <select class="form-control" id="rok" name="rok">
                    <?php
                    for ($tmp = $from; $tmp < $to; $tmp++) { 
                        if ($selected_year == $tmp) {
                            echo "<option selected>$tmp</option>";
                        } else {
                            echo "<option>$tmp</option>";
                        }
                    } 
                    ?>
                </select>
                <button type="submit" class="btn btn-primary" name="action" value="show"><?php text('ukaz');?> </button>
                <button type="submit" class="btn btn-info" name="action" value="edit"><i class="fa fa-pencil"
                                                                                         aria-hidden="true"></i> <?php text('edit');?> 
                </button>
            </div>
        </div>
    </form>

<?php
if ($_GET['action'] == 'edit') { 
    include "editDochadzka.php";
} else {
    include "showDochadzka.php"; 
}
?>

    <div class="panel panel-default">
        <div class="panel-heading"><?php text('legenda');?>:</div> 
        <div class="panel-body">
            <?php
            foreach ($typy_nepritomnosti as $single_typ) {
                echo " <p style='color: " . $single_typ->getFarba() . "'>" . $single_typ->getTyp() . "</p>";
            }
            ?>
        </div>
    </div> 


<?php include('layout/foot.php'); ?>