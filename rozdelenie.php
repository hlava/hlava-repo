<?php
include('layout/head.php');
?>
<div class="table-responsive">
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Meno</th>
            <th>1</th>
            <th>2</th>
            <th>3</th>
            <th>4</th>
            <th>5</th>
            <th>6</th>
            <th>7</th>
            <th>8</th>
            <th>9</th>
            <th>10</th>
            <th>11</th>
            <th>12</th>
            <th>13</th>
            <th>14</th>
            <th>15</th>
            <th>Obsah</th>
            <th>Dizajn</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Tomáš</td>
            <td><i class="fa fa-paw" aria-hidden="true"></i></td>
            <td><i class="fa fa-paw" aria-hidden="true"></i></td>
            <td><i class="fa fa-paw" aria-hidden="true"></i></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td><i class="fa fa-paw" aria-hidden="true"></i></td>
            <td><i class="fa fa-paw" aria-hidden="true"></i></td>
            <td></td>
            <td><i class="fa fa-paw" aria-hidden="true"></i></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>Szabina</td>
            <td><i class="fa fa-paw" aria-hidden="true"></i></td>
            <td><i class="fa fa-paw" aria-hidden="true"></i></td>
            <td><i class="fa fa-paw" aria-hidden="true"></i></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td><i class="fa fa-paw" aria-hidden="true"></i></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td><i class="fa fa-paw" aria-hidden="true"></i></td>
            <td></td>
            <td><i class="fa fa-paw" aria-hidden="true"></i></td>
        </tr>
        <tr>
            <td>Róbert</td>
            <td><i class="fa fa-paw" aria-hidden="true"></i></td>
            <td><i class="fa fa-paw" aria-hidden="true"></i></td>
            <td><i class="fa fa-paw" aria-hidden="true"></i></td>
            <td><i class="fa fa-paw" aria-hidden="true"></i></td>
            <td><i class="fa fa-paw" aria-hidden="true"></i></td>
            <td><i class="fa fa-paw" aria-hidden="true"></i></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td><i class="fa fa-paw" aria-hidden="true"></i></td>
            <td></td>
            <td></td>
            <td><i class="fa fa-paw" aria-hidden="true"></i></td>
            <td><i class="fa fa-paw" aria-hidden="true"></i></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>Máté</td>
            <td><i class="fa fa-paw" aria-hidden="true"></i></td>
            <td><i class="fa fa-paw" aria-hidden="true"></i></td>
            <td><i class="fa fa-paw" aria-hidden="true"></i></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td><i class="fa fa-paw" aria-hidden="true"></i></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td><i class="fa fa-paw" aria-hidden="true"></i></td>
            <td><i class="fa fa-paw" aria-hidden="true"></i></td>
            <td></td>
        </tr>
        <tr>
            <td>Marián</td>
            <td><i class="fa fa-paw" aria-hidden="true"></i></td>
            <td><i class="fa fa-paw" aria-hidden="true"></i></td>
            <td><i class="fa fa-paw" aria-hidden="true"></i></td>
            <td></td>
            <td></td>
            <td></td>
            <td><i class="fa fa-paw" aria-hidden="true"></i></td>
            <td><i class="fa fa-paw" aria-hidden="true"></i></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td><i class="fa fa-paw" aria-hidden="true"></i></td>
            <td></td>
            <td></td>
        </tr>
        </tbody>
    </table>
</div>
<?php include('layout/foot.php'); ?>
