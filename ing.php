<?php
include('layout/head.php');
?>
    <h2>
        Inžinierske štúdium
    </h2><br>
    <div class="well">
        <div class="modal-content" style="padding: 5px;">
    <p><label>Prečo študovať na našom ústave?</label>
        <li>možnosť získať znalosti, ktoré sú implementovateľné v praxi,</li>
        <li>menšie skupiny študentov,</li>
        <li>možnosť dohodnúť si tému pre diplomovku s vybraným pedagógom na základe vlastných preferencií,</li>
        <li>možnosť riešiť diplomovú prácu a teda to, čo každého zaujíma, až 3 semestre,</li>
        <li>pre vynikajúcich študentov možnosť študovať dištančnou metódou</li>
        <li>pre absolventov bakalárskeho štúdia na FEI STU odpustená prijímacia skúška,</li>
        <li>snaha o maximálnu informovanosť študentov prostredníctvom web stránky v dostatočnom predstihu.</li>
    </p>
    <p><label>Nebudem mať problémy, keď som neštudoval mechatroniku aj na bakalárskom štúdiu?</label>
        <li>Mechatronika predstavuje medziodborové štúdium, takže každý by sa tu mal nájsť. Hneď v prvom semestri inžinierskeho štúdia je pre študentov, ktorí predtým neštudovali mechatroniku pripravený vyrovnávací predmet z oblasti automatizácie.</li>
            </p><br> </div></div>
    <div class="well">
        <div class="modal-content" style="padding: 5px;">
    <h2 style="font-weight: bold; font-size: large">Študijný program – 1. ročník</h2><br>
    <p><label>Zimný semester</label>
        <li><strong>CAE mechatronických systémov</strong> - tvorba virtuálnych dynamických modelov a ich simulácia</li>
        <li><strong>Metóda konečných prvkov</strong> - modelovanie a analýza mechatronických prvkov a systémov</li>
        <li><strong>Optimalizácia procesov v mechatronike</strong> – optimalizačné úlohy a metódy v inžinierskych aplikáciách</li>
        <li><strong>Vývojové programové prostredia pre mechatronické systémy</strong> - programovanie mikroprocesorov</li>
        <li><strong>Povinne voliteľný predmet</strong></li>
    </p>
    <p><label>Letný  semester</label>
        <li><strong>Diplomový projekt 1</strong></li>
        <li><strong>Metódy číslicového riadenia</strong> - návrh regulačných obvodov pre modely mechatronických systémov</li>
        <li><strong>Multifyzikálne procesy v mechatronike</strong> – modelovanie tepelných, termoelastických, termoelektrických a piezoelektrických systémov</li>
        <li><strong>Pokročilé informačné technológie</strong> - klient-server aplikácie, riadenie mechatronických systémov v prostredí internetu, Internet vecí (IoT), Industry 4.0</li>
        <li><strong>Povinne voliteľný predmet</strong></li>
            </p></div></div>
    <div class="well">
        <div class="modal-content" style="padding: 5px;">
    <p><i>Možné PVP pre záujemcov o elektroniku</i>
        <li><strong>Inteligentné mechatronické systémy</strong> - implementácia metód výpočtovej a umelej inteligencie pre mechatronické systémy</li>
        <li><strong>MEMS - inteligentné senzory a aktuátory</strong> – najmodernejšie senzory používané nielen v automobilovom priemysle (akcelerometre, gyroskopy, CCD senzory) a spracovanie signálov vnorenými mikropočítačmi </li>
    </p>
    <p><i>Možné PVP pre záujemcov o automobily </i>
        <li><strong>Transmisné systémy automobilov a elektromobilov </strong> - prevodové mechanizmy automobilov a elektromobilov</li>
        <li><strong>Pohonné systémy a zdroje v elektromobiloch </strong> – modelovanie a simulovanie činnosti trakčného a energetického systému elektromobilu</li>
    </p>
    <p><i>Možné PVP pre záujemcov o informatiku </i>
        <li><strong>Inteligentné mechatronické systémy</strong> - implementácia metód výpočtovej a umelej inteligencie pre mechatronické systémy</li>
        <li><strong>Vybrané kapitoly z automatického riadenia pre mechatroniku</strong> – vyrovnávací predmet z automatizácie</li>
        <li><strong>MEMS - inteligentné senzory a aktuátory</strong> – najmodernejšie senzory používané nielen v automobilovom priemysle (akcelerometre, gyroskopy, CCD senzory) a spracovanie signálov vnorenými mikropočítačmi</li>
            </p></div></div>
    <div class="well">
        <div class="modal-content" style="padding: 5px;">
    <a target="_blank" href="files/SP20172018i.pdf">Kompletný študijný plán pre akademický rok 2017-2018</a><br>
    <strong>Prijímacie skúšky na inžinierske štúdium </strong>&nbsp; - &nbsp; 28.6.2017 o 10:00 v D124<br>
    <strong>Prijímacia komisia:</strong><br>
    &nbsp; prof. Ing. Mikuláš Huba, PhD. (predseda)<br>
    &nbsp; prof. Ing. Justín Murín, DrSc. (predseda)<br>
    &nbsp; prof. Ing. Viktor Ferencey, PhD.<br>
    &nbsp; prof. Ing. Štefan Kozák, PhD.<br>
    &nbsp; doc. Ing. Katarína Žáková, PhD.<br>
            <br>Ďalšie informácie na <a target="_blank" href="http://www.mechatronika.cool">http://www.mechatronika.cool</a><br></div></div>

<?php include('layout/foot.php'); ?>