<?php
include('layout/head.php');
?>
    <h2>
        Všeobecné informácie
    </h2>
    <div class="well">
        <div class="modal-content" style="padding: 5px;">
    <label>Harmonogram bakalárskeho štúdia</label><br>
        <i><u>Zimný semester</u></i><br><br>
        <div style="width: 40%;">
            <label style="font-weight: normal;">Začiatok výučby v semestri</label>  <label style="font-weight: normal; float: right">19. 09. 2016</label><br>
            <label style="font-weight: normal;">Prázdniny</label> <label style="font-weight: normal; float: right">31. 10. 2016; 18. 11. 2016</label><br>
            <label style="font-weight: normal;">Začiatok skúškového obdobia</label>  <label style="font-weight: normal; float: right">02. 01. 2017</label><br>
            <label style="font-weight: normal;">Ukončenie skúškového obdobia</label>  <label style="font-weight: normal; float: right">12. 02. 2017</label><br>
        </div><br>

        <i><u>Letný semester</u></i><br><br>
        <div style="width: 40%;">
            <label style="font-weight: normal;">Začiatok výučby v semestri</label>  <label style="font-weight: normal; float: right">13. 02. 2017</label><br>
            <label style="font-weight: normal;">Prázdniny</label> <label style="font-weight: normal; float: right">14. 04. 2017 – 18. 04. 2017</label><br>
            <label style="font-weight: normal;">Začiatok skúškového obdobia</label>  <label style="font-weight: normal; float: right">22. 05. 2017</label><br>
            <label style="font-weight: normal;">Ukončenie skúškového obdobia</label>  <label style="font-weight: normal; float: right">02. 07. 2017</label><br>
        </div><br>
        </div></div>
    <div class="well">
        <div class="modal-content" style="padding: 5px;">
    <i><u>Záver bakalárskeho štúdia</u></i><br><br>
    <div style="width: 40%;">
        <label style="font-weight: normal;">Zadanie záverečnej práce</label>  <label style="font-weight: normal; float: right">13. 02. 2017</label><br>
        <label style="font-weight: normal;">Odovzdanie záverečnej práce</label> <label style="font-weight: normal; float: right">19. 05. 2017</label><br>
        <label style="font-weight: normal;">Štátne skúšky bakalárskeho štúdia</label>  <label style="font-weight: normal; float: right">06. 07. 2017 – 07. 07. 2017</label><br>
        <label style="font-weight: normal;">Promócie absolventov bakalárskeho štúdia </label>  <label style="font-weight: normal; float: right">14. 09. 2017</label><br>
    </div><br></div></div>
    <div class="well">
        <div class="modal-content" style="padding: 5px;">
    <a target="_blank" href="files/SP20162017b.pdf">Študijný plán 2016-2017</a><br>
    <a target="_blank" href="files/studijny_poriadok.pdf">Študijný poriadok</a><br>
            <a target="_blank" href="files/klasifikacna_stupnica.pdf">Klasifikačná stupnica</a></div></div>




<?php include('layout/foot.php'); ?>