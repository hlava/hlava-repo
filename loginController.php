<?php
/**
 * Created by PhpStorm.
 * User: Tavarez
 * Date: 2.4.2017
 * Time: 23:27
 */
include('layout/include_items.php');  
session_start(); 
$email = $_POST["loginEmail"];   
$heslo = $_POST["loginPass"];
if (!isset($_POST["loginEmail"])) { 
    header('Location: dashBoard.php');
} else if(fetchZamestnanecByUid($db, $email) == null){   
redirectToHomeWithMessage(text_without_echo('upozornenieMessage')); 
}
else {
    $dn = 'ou=People, DC=stuba, DC=sk'; 
    $ldaprdn = "uid=" . $email . "," . $dn;
    $ldapconn = ldap_connect("ldap.stuba.sk")
    or die("Could not connect to LDAP server.");
    ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);
    if ($ldapconn) {
        $ldapbind = ldap_bind($ldapconn, $ldaprdn, $heslo);
        if ($ldapbind) {
            $results = ldap_search($ldapconn, $dn, "uid=$email", ["givenname", "employeetype", "surname", "mail", "faculty", "cn", "uisid", "uid"]);
            $info = ldap_get_entries($ldapconn, $results);
            afterLoginMess($email, $info, $db);  
        } else {
            redirectToHomeWithMessage("S Ldapom nie si kamarát :(");
        }
    }
}