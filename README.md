# README #
* vytvorte si novú zložku s názvom projektus kde si naklonujete tento repozitár
* vytvorte si na localhoste databázu s parametrami ako sú v config.php
* keď niečo pridáte do databázy tak vždy pri commite pripojte aj dump.sql a do správy
napíšte ako prvé:
**!need db update**
a ďalej popíšte aké úpravy ste spravili
* keď vytvoríte novú stránku tak ju napojte aj na generovanie navygačného baru 
v layout/include_items.php do poľa $nav_items

### *****************************  new site ************************** ###
* nové stránky vytvárajte nasledovne:


```
#!php

<?php
include('layout/head.php');
?>
       //obsah stránky

<?php include('layout/foot.php'); ?>
```


###****************************  resources/strings.php ************************** ###
* všetky texty ktoré majú mať preklad si vložte sem a ďalej používajte iba ako premenné.
môžete si pridať volitelný parameter pre jednoduchšiu editáciu napr: 

```
#!php

$texts = [
    "linkIs" => [
        "sk" => "AIS STU",
        "en" => "AIS STU",
        "link" => "http://is.stuba.sk"
    ],
    "linkRozvrh" => [
        "sk" => "Rozvrh hodín FEI",
        "en" => "Time table FEI",
        "link" => "http://aladin.elf.stuba.sk/rozvrh/"   --voliteľný parameter
    ],
....
...
];
```
*Pristupujte k stringom cez funkciu function text($key, $opt = null)
kde $key je napr. "linkIs". Rovno to vypisuje (echo nie je nutné použiť) v zvolenom jazyku.
*$opt je voliteľný parameter ak je nadstavený vypisuje parameter. Príklad:
```
#!php
<li><a href="<?php text("linkIs","link");?>" class=""><?php text("linkIs");?></a> </li>
```

*
###*****************************  resources/functions.php ************************** ###
* všetky php funkcie vkladajte sem pre lepší prehľad.
Keď si includnete functions.php alebo založíte stránku ako podla bodu 1 tak budete mať 
k dispozícii všetky funkcie a zároveň aj function dd($variable) kde si môžete ľahko a pekne
formátovane vypísať premennú (dobré pre fast debug)

###*****************************  js scripts ************************** ###
* myslím že bude stačiť js dávať do <script> tagu, poprípade vytvorte podobný js file ako pri
functions.php (nezabudnite ho potom aj includnut v include files)