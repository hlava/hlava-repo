<?php
/**
 * Created by PhpStorm. 
 * User: Tavarez
 * Date: 14.3.2017 
 * Time: 21:11 
 */
include('layout/head.php');
redirectIfNotLogged($user);
?>
<h2> <?php text("welcome"); echo getUserName(); ?></h2>
<div class="container">
    <div class="modal-body">
        <div class="well">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#profil" data-toggle="tab"><?php text("profil");?></a></li>
                <li><a href="#ldap" data-toggle="tab">LDAP</a></li> 
                <li><a href="#role" data-toggle="tab"><?php text("role");?></a></li> 
            </ul>
            <div id="myTabContent" class="tab-content">
                <div class="tab-pane active in" id="profil"> 
                <?php 
                    $dbUser =  getLoggedUserFromDb($db);  
                    $dbUser = fetchZamestnanecByUid($db, $dbUser->getUid()); 
                    ?> 
                    <br>
                    <form method="post" action="editUserProfil.php"> 
                         <input type="hidden" name="userId" value="<?php echo $dbUser->getId();?>" >    
                         <button type="submit" class="btn btn-info"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Edituj profil</button> 
                    </form>    
                    <br>
                <?php  
                foreach ($dbUser as $key => $itemus) {
                                echo "<strong>" . ucwords($key) . "</strong>: ";              
                                echo $itemus;
                                echo "<br>";
                             }?>
                 </div>
                    <div class="tab-pane" id="ldap">
                    <br>
                    <p> 
                    <?php
                     $userFromDb = getLoggedUserFromDb($db);  
                    $variables = $_SESSION['variables'];
                    foreach ($variables[0] as $key => $item) { 
                        $tmpFirstStep = true;
                        foreach ($item as $itemus) {
                            if ($tmpFirstStep) {
                                $tmpFirstStep = false;
                                echo "<strong>" . ucwords($key) . "</strong>: ";
                            } else {
                                echo $itemus;
                                echo "  ";
                            }
                        }
                        echo "<br>";
                    }
                   // dd($variables);
                    ?>
                    </p> 
                </div> 
                <div class="tab-pane" id="role">
                <ul> 
                <br>
                <?php
                 if($userFromDb->getAdmin()){
                                        ?> 
                                <li><a href="editRoles.php"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> <?php text("editRoles");?></a>
                                        <?php
                                    }
                                ?>
                <hr>
                <li>     
User: <?php if ($userFromDb->getUser()) { echo "<i class='fa fa-check' aria-hidden='true'></i>";} else {echo "<i class='fa fa-times' aria-hidden='true'></i>";} ?> 
</li>
 
<li>
HR:  <?php if ($userFromDb->getHr()) { echo "<i class='fa fa-check' aria-hidden='true'></i>";} else {echo "<i class='fa fa-times' aria-hidden='true'></i>";} ?> 
</li>
 
<li>
Reporter: <?php if ($userFromDb->getReporter()) { echo "<i class='fa fa-check' aria-hidden='true'></i>";} else {echo "<i class='fa fa-times' aria-hidden='true'></i>";} ?> 
</li> 
 
<li>
Editor:<?php if ($userFromDb->getEditor()) { echo "<i class='fa fa-check' aria-hidden='true'></i>";} else {echo "<i class='fa fa-times' aria-hidden='true'></i>";} ?> 
</li>
 
<li>
Admin: <?php if ($userFromDb->getAdmin()) { echo "<i class='fa fa-check' aria-hidden='true'></i>";} else {echo "<i class='fa fa-times' aria-hidden='true'></i>";} ?> 
</li>
 
                             </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include('layout/foot.php'); ?>
