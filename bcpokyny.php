<?php
include('layout/head.php');
?>
    <h2>
        Pokyny
    </h2>
    <div class="well">
        <div class="modal-content" style="padding: 5px;">
    <label>Ukončovanie predmetov BP1, BP2, BZP</label><br>
            <div class="panel panel-info">
                <div class="panel-heading"><i><u>Bakalársky projekt 1</u></i><br><br></div>
                <div class="panel-body"> <div style="width: 50%;">
        <label style="font-weight: normal;">Zodpovedný:</label>  <label style="font-weight: normal; float: right">doc. Ing. Vladimír Kutiš, PhD.</label><br>
        <label style="font-weight: normal;">Hodnotenie predmetu: </label> <label style="font-weight: normal; float: right">klasifikovaný zápočet</label><br>
        <label style="font-weight: normal;">Štandardný čas plnenia: </label>  <label style="font-weight: normal; float: right">3. roč. bakalárskeho štúdia, zimný semester</label><br>
        <label style="font-weight: normal;">Pre získanie klasifikovaného zápočtu musí študent odovzdať technickú dokumentáciu svojmu vedúcemu práce v nim špecifikovanom rozsahu najneskôr do 20.januára daného roku. Prácu na projekte hodnotí vedúci práce.</label>
                    </div></div></div><br>

                <div class="panel panel-info">
                    <div class="panel-heading"><i><u>Bakalársky projekt 2</u></i><br><br></div>
    <div style="width: 50%;">
        <label style="font-weight: normal;">Zodpovedný:</label>  <label style="font-weight: normal; float: right">doc. Ing. Vladimír Kutiš, PhD.</label><br>
        <label style="font-weight: normal;">Hodnotenie predmetu: </label> <label style="font-weight: normal; float: right">klasifikovaný zápočet</label><br>
        <label style="font-weight: normal;">Štandardný čas plnenia: </label>  <label style="font-weight: normal; float: right">3. roč. bakalárskeho štúdia, letný semester</label><br>
        <label style="font-weight: normal;">Pre získanie klasifikovaného zápočtu musí študent do dátumu špecifikovanom v harmonograme štúdia FEI STU odovzdať bakalársku prácu:<br>1. v elektronickej forme do AIS<br>2. v tlačenej forme v počte 2 kusy Ing. Sedlárovi (A803)<br>alebo odovzdať technickú dokumentáciu svojmu vedúcemu práce v nim špecifikovanom rozsahu najneskôr do 20.júna daného roku.<br>Prácu na projekte hodnotí vedúci práce.</label>
    </div><br></div>

                    <div class="panel panel-info">
                        <div class="panel-heading"><i><u>Bakalárska záverečná práca</u></i><br><br></div>
    <div style="width: 50%;">
        <label style="font-weight: normal;">Zodpovedný:</label>  <label style="font-weight: normal; float: right">doc. Ing. Vladimír Kutiš, PhD.</label><br>
        <label style="font-weight: normal;">Hodnotenie predmetu: </label> <label style="font-weight: normal; float: right">klasifikovaný zápočet</label><br>
        <label style="font-weight: normal;">Štandardný čas plnenia: </label>  <label style="font-weight: normal; float: right">3. roč. bakalárskeho štúdia, letný semester</label><br>
        <label style="font-weight: normal;">Pre získanie skúšky musí študent obhájiť tému svojej diplomovej práce pred štátnicovou komisiou, ktorá zároveň udeľuje známku za obhajobu.</label>
    </div><br></div></div></div>


<?php include('layout/foot.php'); ?>