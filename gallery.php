<?php
include('layout/head.php');
?>
    <h2>
        <i class="fa fa-child" aria-hidden="true"></i> <?php text("galeria");?> <i class="fa fa-child" aria-hidden="true"></i>
    </h2>
<style>
div.gallery {
    border: 1px solid #ccc;
}

div.gallery:hover {
    border: 1px solid #777;
}

div.gallery img {
    width: 100%;
    height: auto;
}

div.desc {
    padding: 15px;
    text-align: center;
}

* {
    box-sizing: border-box;
}

.responsive {
    padding: 0 6px;
    float: left;
    width: 24.99999%;
}

@media only screen and (max-width: 700px){
    .responsive {
        width: 49.99999%;
        margin: 6px 0;
    }
}

@media only screen and (max-width: 500px){
    .responsive {
        width: 100%;
    }
}

.clearfix:after {
    content: "";
    display: table;
    clear: both;
}
 
div.title {
    padding: 9px; 
    text-align: center;
}
div.desc {
    padding: 4px;
    text-align: center;
    font-size: 65%;
}
</style> 
<?php
$galerie = getGalleries($db);
foreach ($galerie as $gall) { 
    ?>
 
    <div class="responsive">
  <div class="gallery">
    <a href="/projektus/gallerySingle.php?choosenGallery=<?php echo $gall->getId(); ?>">
      <img src="images/gallery.jpg" alt="Trolltunga Norway" width="300" height="200">
    </a>
    <div class="title"> <?php echo $gall->getTitle($lang); ?></div>
    <div class="desc"> <?php echo $gall->getDescr($lang); ?></div>
  </div>
</div>

    <?php 
}
?>
<?php include('layout/foot.php'); ?>