<?php
include('layout/head.php');
?>
<h2>
    <i class="fa fa-child" aria-hidden="true"></i> <?php text("kontaktTitle"); ?> <i class="fa fa-child" aria-hidden="true"></i>
</h2>
<div class="col-md-6">
    <h2 style="font-size: x-large"><?php text("kontaktAdresa"); ?></h2>
    <address>
        <?php text("kontaktInstitute"); ?>,<br>
        FEI STU,<br>
        Ilkovičova 3,<br>
        812 19 Bratislava,<br>
        <?php text("kontaktSlovakia"); ?><br>
    </address>

    <h2 style="font-size: x-large"><?php text("kontaktSekretariat"); ?></h2>
    <address>
        Katarína Kermietová<br>
        <?php text("kontaktTelephone"); ?>: +421 (2) 60 291 598<br>
        <?php text("kontaktPracovisko"); ?>: ÚAM FEI<br>
        <?php text("kontaktMiestnost"); ?>: D116<br>
    </address> 
</div>
<div class="col-md-6">
<div id="googleMap1" style="width:100%;height:auto; min-height: 500px;"></div>    
</div>
<script src="js/mapascript.js"></script>
<script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA5z_y1bwsUTPqC1yBFCOGeP37-tvKaeJw&libraries=places&callback=myMap"
    type="text/javascript"></script>


<?php include('layout/foot.php'); ?>

