<?php
include('layout/head.php');
?>
<h2>
    Čo je to mechatronika?
</h2>
<div class="well">
    <div class="modal-content" style="padding: 5px;">
<p style = "text-indent: 30px;">
    Mechatronika predstavuje inžiniersky odbor, ktorý stojí na rozhraní viacerých oblastí výskumu, vývoja a aplikácií. Kombinuje prvky mechaniky, elektroniky, automatizácie a výpočtovej techniky. Typický mechatronický systém príjima
    pomocou rôznych typov senzorov signály z vonkajšieho prostredia, ktoré spracuje a transformuje na vykonanie
    potrebnej sily, pohybu alebo iného typu akčného zásahu. Umožňuje rozšírenie mechanického systému o senzory a
    mikropočítače. Mechatronický systém je riadený softvérovým vybavením, ktoré je neoddeliteľnou súčasťou produktu a
    nevyhnutné pre jeho správne fungovanie.</p></div></div>

<?php include('layout/foot.php'); ?>
