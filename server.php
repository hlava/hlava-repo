<?php include('layout/include_items.php');

include('config/db_majo.php');

$photo = $_POST['photo'];
$department = $_POST['department']."  ";
$phone = $_POST['phone']."  ";
$room = $_POST['room']."  ";
$url = $_POST['url'];
$meno = $_POST['meno'];


$sql = "SELECT celynazov FROM oddelenia WHERE skratka = '".$department."';";
$result = $conn->query($sql);

if (($result->num_rows) > 0) {
    $row = $result->fetch_assoc();
    $department=$row["celynazov"];
}

if (!$photo) $photo="Default.JPG";
echo "<img src='files/foto/staff_photo/".$photo."' alt='Photo' height='30%' width='30%'>";

echo '<table class="table table-striped table-bordered"><thead>';
echo "<tr class='bg-primary'><th colspan='2'>$meno</th></tr></thead>";

echo "<tbody><tr>";
?>
    <tr><td><?php text(zam_oddelenie) ?></td><td><?php echo $department ?></td></tr>
    <tr><td><?php text(zam_telefon) ?></td><td>+421 2 60291<?php echo $phone?></td></tr>
    <tr><td><?php text(zam_miestnost)?></td><td><?php echo $room ?></td></tr>

<?php

echo '</tr></tbody></table>';
?>

<div class="panel-group" id="accordion">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
            <?php text(publikacie);?>
</a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse">
      <div class="panel-body">

<?php

if ($lang == "sk") {

    $url = $url . ";lang=sk;zalozka=5;rok=1;order_by=rok_uplatneni;";

}
else {
    $url = $url . ";lang=en;zalozka=5;rok=1;order_by=rok_uplatneni;";
}
$ch = curl_init($url);
// nastavenie curl-u pre ulozenie dat z odpovede do navratovej hodnoty z volania curl_exec
curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

// vykonanie dopytu
$result = curl_exec($ch);
curl_close($ch);


if ($result) {

    $doc = new DOMDocument();
    libxml_use_internal_errors(true);
    $doc->loadHTML($result);
    $xPath = new DOMXPath($doc);
    $tableRows = $xPath->query('//table[3]/tbody/tr');
echo'<div class="table-responsive">';
    echo '<table class="table table-bordered" >'; ?>
    <tr class="bg-primary">
        <th><?php text(publikacia); ?> </th>
        <th><?php text(druh_publikacie); ?></th>
        <th><?php text(rok); ?></th>
    </tr>

    <?php
    if ($lang == "sk") {
        $string = "články včasopisoch";
        $string = htmlentities($string, null, 'utf-8');
        $string = str_replace("&nbsp;", "", $string);
        //($string);

        $string2 = "príspevky vzborníkoch, kapitoly vmonografiách/učebniciach, abstrakty";
        $string2 = htmlentities($string2, null, 'utf-8');
        $string2 = str_replace("&nbsp;", "", $string2);

        for ($x = 0; $x < 3; $x++) {
            for ($i = 0; $i < $tableRows->length; $i++) {
                $p = $tableRows[$i]->childNodes[2]->textContent;

                $p = htmlentities($p, null, 'utf-8');
                $p = str_replace("&nbsp;", "", $p);

                if ($tableRows[$i]->childNodes[3]->textContent > 2012) {
                    if (($x == 0) && ($tableRows[$i]->childNodes[2]->textContent == "monografie, učebnice, skriptá, príručky, normy, patenty, výskumné správy, iné neperiodické publikácie")) {
                        echo "<tr class='bg-info'><td>" . $tableRows[$i]->childNodes[1]->textContent . "</td>";
                        echo "<td>" . $tableRows[$i]->childNodes[2]->textContent . "</td>";
                        echo "<td>" . $tableRows[$i]->childNodes[3]->textContent . "</td></tr>";
                    }
                    if (($x == 1) && ($p == $string)) {
                        echo "<tr class='bg-danger'><td>" . $tableRows[$i]->childNodes[1]->textContent . "</td>";
                        echo "<td>" . $tableRows[$i]->childNodes[2]->textContent . "</td>";
                        echo "<td>" . $tableRows[$i]->childNodes[3]->textContent . "</td></tr>";
                    }
                    if (($x == 2) && ($p == $string2)) {
                        echo "<tr class='bg-success'><td>" . $tableRows[$i]->childNodes[1]->textContent . "</td>";
                        echo "<td>" . $tableRows[$i]->childNodes[2]->textContent . "</td>";
                        echo "<td>" . $tableRows[$i]->childNodes[3]->textContent . "</td></tr>";
                    }
                }

            }
        }
    } else {
        for ($x = 0; $x < 3; $x++) {
            for ($i = 0; $i < $tableRows->length; $i++) {
                $p = $tableRows[$i]->childNodes[2]->textContent;

                if ($tableRows[$i]->childNodes[3]->textContent > 2012) {
                    if (($x == 0) && ($tableRows[$i]->childNodes[2]->textContent == "monographs, textbooks, manuals, norms, patents, research reports, other non-periodical publications")) {
                        echo "<tr class='bg-info'><td>" . $tableRows[$i]->childNodes[1]->textContent . "</td>";
                        echo "<td>" . $tableRows[$i]->childNodes[2]->textContent . "</td>";
                        echo "<td>" . $tableRows[$i]->childNodes[3]->textContent . "</td></tr>";
                    }
                    if (($x == 1) && ($p == "articles in magazines")) {
                        echo "<tr class='bg-danger'><td>" . $tableRows[$i]->childNodes[1]->textContent . "</td>";
                        echo "<td>" . $tableRows[$i]->childNodes[2]->textContent . "</td>";
                        echo "<td>" . $tableRows[$i]->childNodes[3]->textContent . "</td></tr>";
                    }
                    if (($x == 2) && ($p == "contributions in anthologies, chapters in monographs/textbooks, abstracts")) {
                        echo "<tr class='bg-success'><td>" . $tableRows[$i]->childNodes[1]->textContent . "</td>";
                        echo "<td>" . $tableRows[$i]->childNodes[2]->textContent . "</td>";
                        echo "<td>" . $tableRows[$i]->childNodes[3]->textContent . "</td></tr>";
                    }
                }

            }
        }
    }
}
?>
</table>
      </div>

      </div>
    </div>
  </div>

