-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Hostiteľ: localhost
-- Čas generovania: Pi 26.Máj 2017, 19:37
-- Verzia serveru: 5.7.17-0ubuntu0.16.04.1
-- Verzia PHP: 7.0.15-0ubuntu0.16.04.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáza: `projectus`
--

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `aktuality`
--

CREATE TABLE `aktuality` (
  `slov` varchar(200) CHARACTER SET utf8 NOT NULL,
  `eng` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `datum` date DEFAULT NULL,
  `typ` tinyint(4) NOT NULL,
  `nazov` varchar(40) CHARACTER SET utf8 DEFAULT NULL,
  `title` varchar(40) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Sťahujem dáta pre tabuľku `aktuality`
--

INSERT INTO `aktuality` (`slov`, `eng`, `datum`, `typ`, `nazov`, `title`) VALUES
('Máš záujem o informatiku, elektronické systémy, mechaniku a\r\nautomatické riadenie? Všetky tieto oblasti môžeš študovať\r\nsúčasne v jednom študijnom programe.', '', '2017-07-31', 1, 'Čím sa môžem stať?', ''),
('Rozbehli sme súťaž pre stredoškolákov o kvalitné tablety! Tak neváhaj a zapoj sa! :) Minule vyhrala šikovná študentka tiež výkonný tablet a teraz študuje na STU.', '', '2017-06-25', 3, 'Súťaž o 3 výkonné tablety', ''),
('Chceme vás informovať o štúdiu na odbore mechatronika, ukázať vám priestory našej fakulty a porozprávať o tom, aké je to byť „FEIkárom“.\r\nDozviete sa všetko čo potrebujete vedieť o študijnom programe.', '', '2017-02-07', 2, 'Deň otvorených dverí', '');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `galeria`
--

CREATE TABLE `galeria` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(60) CHARACTER SET utf8 NOT NULL,
  `descr` varchar(250) CHARACTER SET utf8 NOT NULL,
  `titleEn` varchar(60) NOT NULL,
  `descrEn` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Sťahujem dáta pre tabuľku `galeria`
--

INSERT INTO `galeria` (`id`, `title`, `descr`, `titleEn`, `descrEn`) VALUES
(1, 'Deň otvorených dverí na ÚAMT FEI STU', 'Konané dňa 07.02.2017', 'Open day at UAMT FEI STU', 'At 07.02.2017'),
(2, 'Noc výskumníkov', 'Konané dňa 25.09.2015', 'Night of researchers', 'At 25.09.2015');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `galeriaSingle`
--

CREATE TABLE `galeriaSingle` (
  `id` int(10) UNSIGNED NOT NULL,
  `fk_galery` int(10) UNSIGNED NOT NULL,
  `title` varchar(60) CHARACTER SET utf8 NOT NULL,
  `descr` varchar(250) CHARACTER SET utf8 NOT NULL,
  `path` varchar(250) NOT NULL,
  `path_thumbnail` varchar(250) NOT NULL,
  `titleEn` varchar(60) NOT NULL,
  `descrEn` varchar(250) NOT NULL,
  `size` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Sťahujem dáta pre tabuľku `galeriaSingle`
--

INSERT INTO `galeriaSingle` (`id`, `fk_galery`, `title`, `descr`, `path`, `path_thumbnail`, `titleEn`, `descrEn`, `size`) VALUES
(1, 1, 'foto1 (1).JPG SK', 'Popis', 'files/foto/album1/foto1 (1).JPG', 'files/foto/album1/thumb/foto1 (1)_tn.jpg', 'foto1 (1).JPG EN', 'en descr', '2136x1424'),
(2, 1, 'foto1 (2).JPG', 'Popis', 'files/foto/album1/foto1 (2).JPG', 'files/foto/album1/thumb/foto1 (2)_tn.jpg', 'foto1 (2).JPG EN', 'en descr', '2136x1424'),
(3, 1, 'foto1 (3).JPG', 'Popis', 'files/foto/album1/foto1 (3).JPG', 'files/foto/album1/thumb/foto1 (3)_tn.jpg', 'foto1 (3).JPG EN', 'en descr', '2136x1424'),
(4, 1, 'foto1 (4).JPG', 'Popis', 'files/foto/album1/foto1 (4).JPG', 'files/foto/album1/thumb/foto1 (4)_tn.jpg', 'foto1 (3).JPG EN', 'en descr', '2136x1424'),
(5, 1, 'foto1 (5).JPG', 'Popis', 'files/foto/album1/foto1 (5).JPG', 'files/foto/album1/thumb/foto1 (5)_tn.jpg', 'foto1 (5).JPG EN', 'en descr', '2136x1424'),
(6, 1, 'foto1 (6).JPG', 'Popis', 'files/foto/album1/foto1 (6).JPG', 'files/foto/album1/thumb/foto1 (6)_tn.jpg', 'foto1 (6).JPG EN', 'en descr', '2136x1424'),
(7, 2, 'sk 1', 'Popis', 'files/foto/album2/foto2 (1).JPG', 'files/foto/album2/thumb/foto2 (1)_tn.jpg', 'en 1', 'en descr', '2136x1424'),
(8, 2, 'sk 4', 'Popis', 'files/foto/album2/foto2 (4).jpg', 'files/foto/album2/thumb/foto2 (4)_tn.jpg', 'en 4', 'en descr', '1424x2136'),
(9, 2, 'sk 5', 'Popis', 'files/foto/album2/foto2 (5).jpg', 'files/foto/album2/thumb/foto2 (5)_tn.jpg', 'en 5', 'en descr', '2136x1424'),
(10, 2, 'sk 6', 'Popis', 'files/foto/album2/foto2 (6).jpg', 'files/foto/album2/thumb/foto2 (6)_tn.jpg', 'en 6', 'en descr', '2136x1424'),
(11, 2, 'sk 7', 'Popis', 'files/foto/album2/foto2 (7).jpg', 'files/foto/album2/thumb/foto2 (7)_tn.jpg', 'en 7', 'en descr', '2136x1424'),
(12, 2, 'sk 2', 'Popis', 'files/foto/album2/foto2 (2).jpg', 'files/foto/album2/thumb/foto2 (2)_tn.jpg', 'en 2', 'en descr', '2136x1424'),
(13, 2, 'sk 3', 'Popis', 'files/foto/album2/foto2 (3).jpg', 'files/foto/album2/thumb/foto2 (3)_tn.jpg', 'en 3', 'en descr', '2136x1424'),
(14, 2, 'sk 8', 'Popis', 'files/foto/album2/foto2 (8).jpg', 'files/foto/album2/thumb/foto2 (8)_tn.jpg', 'en 8', 'en descr', '2136x1424'),
(15, 2, 'sk 9', 'Popis', 'files/foto/album2/foto2 (9).jpg', 'files/foto/album2/thumb/foto2 (9)_tn.jpg', 'en 9', 'en descr', '2136x1424'),
(16, 2, 'sk 10', 'Popis', 'files/foto/album2/foto2 (10).jpg', 'files/foto/album2/thumb/foto2 (10)_tn.jpg', 'en 10', 'en descr', '2136x1424'),
(17, 2, 'sk 11', 'Popis', 'files/foto/album2/foto2 (11).jpg', 'files/foto/album2/thumb/foto2 (11)_tn.jpg', 'en 11', 'en descr', '2136x1424');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `login`
--

CREATE TABLE `login` (
  `id` int(10) UNSIGNED NOT NULL,
  `uid` varchar(60) CHARACTER SET utf8 COLLATE utf8_slovak_ci NOT NULL,
  `user` tinyint(1) DEFAULT '1',
  `hr` tinyint(1) DEFAULT '0',
  `reporter` tinyint(1) DEFAULT '0',
  `editor` tinyint(1) DEFAULT '0',
  `admin` tinyint(1) DEFAULT '0',
  `meno` varchar(100) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Sťahujem dáta pre tabuľku `login`
--

INSERT INTO `login` (`id`, `uid`, `user`, `hr`, `reporter`, `editor`, `admin`, `meno`) VALUES
(4, 'xfilo', 1, 0, 1, 0, 1, 'Robert Filo'),
(5, 'bistak', 1, 1, 0, 1, 1, 'Bistak'),
(6, 'xgulam', 0, 0, 0, 1, 1, 'Gula'),
(7, 'xkocurm2', 1, 0, 0, 0, 1, 'Kocur'),
(8, 'xrabek', 1, 0, 0, 0, 1, 'Rabek'),
(9, 'zakova', 1, 0, 0, 0, 1, 'Zakova'),
(22, 'xhajek', 1, 0, 0, 0, 1, 'Stanislav Hajek'),
(23, 'xkrkoska', 1, 0, 0, 0, 0, 'Marian Krkoska'),
(24, 'xbaraniak', 1, 0, 1, 0, 0, 'Tomas Baraniak'),
(25, 'xbucsai', 1, 0, 1, 0, 1, 'Szabina Bucsai');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `media`
--

CREATE TABLE `media` (
  `id` int(1) DEFAULT NULL,
  `title` varchar(61) DEFAULT NULL,
  `media` varchar(18) DEFAULT NULL,
  `date` varchar(10) DEFAULT NULL,
  `pdf` varchar(19) DEFAULT NULL,
  `url` varchar(134) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Sťahujem dáta pre tabuľku `media`
--

INSERT INTO `media` (`id`, `title`, `media`, `date`, `pdf`, `url`) VALUES
(1, 'Študenti z Bratislavy vyvinuli u nás prvú elektrickú motokáru', 'Hospodárske noviny', '2014-10-14', '', 'http://dennik.hnonline.sk/ekonomika-a-firmy/591621-studenti-z-bratislavy-vyvinuli-u-nas-prvu-elektricku-motokaru#.VETnmBjntpk.facebook'),
(2, 'Prvá elektrická motokára na Slovensku vznikla v škole', 'Pravda', '2014-10-20', '', 'http://spravy.pravda.sk/ekonomika/clanok/333718-prva-elektricka-motokara-na-slovensku-vznikla-v-skole/'),
(3, 'Poodkryl tajomstvo', 'Šarm', '2015-11-10', 'sarm201546.pdf', ''),
(4, 'Mladí vedci navrhli snímač akupunktúrnych bodov', 'Rádio Regina', '2016-01-19', '', 'http://reginazapad.rtvs.sk/clanky/deti/98134/mladi-vedci-navrhli-snimac-akupunkturnych-bodov'),
(5, 'Vďaka biomechatronikom z STU sa už akupunktúrne body neskryjú', 'Dennik N', '2016-03-29', 'science20162903.pdf', 'http://science.dennikn.sk/clankyarozhovory/'),
(6, 'Automobilová mechatronika (od 6:35 min)', 'VAT RTVS', '2017-01-12', '', 'https://www.rtvs.sk/televizia/archiv/11767/115433'),
(7, 'Prvý slovenský elektrický skúter (od 7:50 min)', 'VAT RTVS', '2017-01-19', '', 'https://www.rtvs.sk/televizia/archiv/11767/117377');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `nepritomnost`
--

CREATE TABLE `nepritomnost` (
  `id` int(11) NOT NULL,
  `zamestnanec` int(11) NOT NULL,
  `typ_nepritomnosti` int(11) NOT NULL,
  `datum` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Sťahujem dáta pre tabuľku `nepritomnost`
--

INSERT INTO `nepritomnost` (`id`, `zamestnanec`, `typ_nepritomnosti`, `datum`) VALUES
(110, 2, 1, '2017-03-07'),
(111, 2, 1, '2017-03-09'),
(112, 4, 1, '2017-03-09'),
(115, 2, 3, '2017-01-07'),
(116, 1, 3, '2017-01-08'),
(117, 3, 3, '2017-01-11'),
(132, 1, 1, '2032-09-09'),
(133, 1, 1, '2032-09-15'),
(134, 2, 1, '2017-06-05'),
(135, 2, 1, '2017-06-15'),
(136, 4, 1, '2017-06-10'),
(137, 1, 1, '2017-06-07'),
(151, 4, 5, '2017-02-28'),
(156, 2, 5, '2017-02-27'),
(157, 2, 5, '2017-02-28'),
(167, 4, 1, '2018-12-12'),
(168, 4, 1, '2018-12-15'),
(169, 4, 1, '2018-12-21'),
(170, 4, 1, '2018-12-22'),
(171, 4, 1, '2018-12-25'),
(172, 4, 1, '2018-12-27'),
(209, 2, 5, '2023-01-06'),
(210, 2, 5, '2023-01-11'),
(211, 2, 5, '2023-01-12'),
(212, 4, 5, '2023-01-11'),
(213, 1, 5, '2023-01-03'),
(214, 1, 1, '2023-01-04'),
(215, 1, 1, '2023-01-05'),
(216, 1, 1, '2023-01-06'),
(217, 1, 1, '2023-01-07'),
(218, 1, 5, '2023-01-08'),
(219, 1, 5, '2023-01-09'),
(220, 1, 5, '2023-01-10'),
(221, 1, 1, '2023-01-11'),
(306, 1, 2, '2017-02-14'),
(314, 1, 5, '2017-02-26'),
(316, 2, 2, '2022-06-11'),
(317, 2, 2, '2022-06-13'),
(318, 2, 2, '2022-06-15'),
(319, 2, 2, '2022-06-25'),
(320, 2, 2, '2022-06-28'),
(321, 4, 4, '2022-06-09'),
(322, 4, 4, '2022-06-13'),
(323, 1, 4, '2022-06-07'),
(324, 1, 4, '2022-06-12'),
(334, 4, 5, '2017-02-25'),
(335, 4, 5, '2017-02-26'),
(344, 1, 2, '2017-02-23'),
(346, 1, 5, '2017-02-25'),
(348, 1, 5, '2017-02-28'),
(362, 3, 5, '2017-02-28'),
(373, 1, 3, '2017-02-02'),
(386, 4, 3, '2017-06-12'),
(387, 4, 3, '2017-06-13'),
(388, 2, 2, '2020-01-06'),
(389, 4, 2, '2020-01-08'),
(401, 2, 1, '2017-05-01'),
(402, 2, 1, '2017-05-02'),
(405, 2, 1, '2017-05-06'),
(406, 2, 1, '2017-05-09'),
(407, 2, 1, '2017-05-11'),
(408, 2, 1, '2017-05-12'),
(409, 2, 1, '2017-05-13'),
(410, 2, 1, '2017-05-14'),
(411, 2, 1, '2017-05-18'),
(412, 4, 1, '2017-05-01'),
(413, 4, 1, '2017-05-04'),
(414, 4, 4, '2017-05-06'),
(415, 4, 4, '2017-05-07'),
(416, 4, 4, '2017-05-08'),
(420, 4, 1, '2017-05-14'),
(421, 4, 1, '2017-05-18'),
(422, 1, 1, '2017-05-01'),
(423, 1, 1, '2017-05-02'),
(425, 1, 1, '2017-05-04'),
(426, 1, 1, '2017-05-06'),
(427, 1, 1, '2017-05-09'),
(428, 1, 1, '2017-05-11'),
(430, 1, 1, '2017-05-14'),
(431, 1, 1, '2017-05-16'),
(432, 1, 1, '2017-05-18'),
(433, 3, 1, '2017-05-01'),
(434, 3, 1, '2017-05-04'),
(435, 3, 1, '2017-05-06'),
(436, 3, 1, '2017-05-07'),
(437, 3, 1, '2017-05-09'),
(438, 3, 1, '2017-05-11'),
(439, 3, 1, '2017-05-12'),
(440, 3, 1, '2017-05-13'),
(441, 3, 1, '2017-05-14'),
(442, 3, 1, '2017-05-17'),
(450, 2, 1, '2017-03-14'),
(451, 2, 1, '2017-03-19'),
(452, 2, 1, '2017-03-20'),
(453, 2, 1, '2017-03-21'),
(454, 4, 1, '2017-03-12'),
(455, 4, 1, '2017-03-13'),
(456, 4, 1, '2017-03-14'),
(457, 4, 1, '2017-03-15'),
(458, 4, 1, '2017-03-16'),
(459, 4, 1, '2017-03-17'),
(460, 4, 1, '2017-03-18'),
(462, 4, 1, '2017-02-03'),
(474, 2, 1, '2018-01-01'),
(475, 2, 1, '2018-01-02'),
(476, 2, 1, '2018-01-03'),
(477, 2, 3, '2018-01-04'),
(481, 2, 1, '2018-01-08'),
(482, 2, 1, '2018-01-09'),
(485, 2, 3, '2018-01-05'),
(486, 2, 1, '2018-01-06'),
(487, 2, 1, '2018-01-07'),
(488, 4, 3, '2018-01-04'),
(489, 4, 3, '2018-01-05'),
(490, 4, 3, '2018-01-06'),
(491, 4, 3, '2018-01-07'),
(492, 4, 3, '2018-01-08'),
(493, 4, 3, '2018-01-09'),
(494, 4, 3, '2018-01-10'),
(495, 4, 3, '2018-01-11'),
(496, 1, 4, '2018-01-01'),
(497, 1, 4, '2018-01-02'),
(498, 1, 2, '2018-01-03'),
(499, 1, 2, '2018-01-04'),
(500, 1, 2, '2018-01-05'),
(501, 1, 2, '2018-01-06'),
(504, 3, 2, '2018-01-03'),
(505, 3, 2, '2018-01-04'),
(506, 3, 2, '2018-01-05'),
(507, 3, 2, '2018-01-06'),
(508, 3, 2, '2018-01-07'),
(509, 3, 2, '2018-01-08'),
(512, 3, 2, '2018-01-11'),
(513, 3, 2, '2018-01-12'),
(514, 3, 2, '2018-01-13'),
(516, 2, 3, '2017-02-01'),
(520, 2, 3, '2017-02-05'),
(521, 5, 1, '2017-02-08'),
(522, 5, 1, '2017-02-09'),
(523, 42, 1, '2017-05-18'),
(524, 4, 4, '2017-05-05'),
(525, 4, 4, '2017-05-10'),
(526, 4, 4, '2017-05-11'),
(527, 46, 1, '2017-02-08'),
(528, 46, 1, '2017-02-09'),
(529, 46, 1, '2017-02-10'),
(530, 46, 1, '2017-02-11');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `newsletter`
--

CREATE TABLE `newsletter` (
  `email` varchar(40) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Sťahujem dáta pre tabuľku `newsletter`
--

INSERT INTO `newsletter` (`email`) VALUES
('sz1@mail.com'),
('bucsaiszabina@gmail.com');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `oddelenia`
--

CREATE TABLE `oddelenia` (
  `skratka` varchar(4) COLLATE utf8_slovak_ci DEFAULT NULL,
  `celynazov` varchar(59) COLLATE utf8_slovak_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovak_ci;

--
-- Sťahujem dáta pre tabuľku `oddelenia`
--

INSERT INTO `oddelenia` (`skratka`, `celynazov`) VALUES
('AHU', 'Administratívno-hospodársky úsek'),
('OAMM', 'Oddelenie aplikovanej mechaniky a mechatroniky'),
('OIKR', 'Oddelenie informačných, komunikačných a riadiacich systémov'),
('OEMP', 'Oddelenie elektroniky, mikropočítačov a PLC systémov'),
('OEAP', 'Oddelenie E-mobility, automatizácie a pohonov');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `projekty`
--

CREATE TABLE `projekty` (
  `id` int(2) DEFAULT NULL,
  `skupina` int(1) DEFAULT NULL,
  `projectType` varchar(60) COLLATE utf8_slovak_ci DEFAULT NULL,
  `number` varchar(15) COLLATE utf8_slovak_ci DEFAULT NULL,
  `titleSK` varchar(118) COLLATE utf8_slovak_ci DEFAULT NULL,
  `titleEN` varchar(126) COLLATE utf8_slovak_ci DEFAULT NULL,
  `from` varchar(9) COLLATE utf8_slovak_ci DEFAULT NULL,
  `to` varchar(10) COLLATE utf8_slovak_ci DEFAULT NULL,
  `years_from` int(4) DEFAULT NULL,
  `years_to` int(4) DEFAULT NULL,
  `order_by` varchar(10) COLLATE utf8_slovak_ci DEFAULT NULL,
  `duration` varchar(21) COLLATE utf8_slovak_ci DEFAULT NULL,
  `coordinator` varchar(51) COLLATE utf8_slovak_ci DEFAULT NULL,
  `partners` varchar(158) COLLATE utf8_slovak_ci DEFAULT NULL,
  `web` varchar(33) COLLATE utf8_slovak_ci DEFAULT NULL,
  `internalCode` varchar(4) COLLATE utf8_slovak_ci DEFAULT NULL,
  `annotationSK` varchar(2018) COLLATE utf8_slovak_ci DEFAULT NULL,
  `annotationEN` varchar(2030) COLLATE utf8_slovak_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovak_ci;

--
-- Sťahujem dáta pre tabuľku `projekty`
--

INSERT INTO `projekty` (`id`, `skupina`, `projectType`, `number`, `titleSK`, `titleEN`, `from`, `to`, `years_from`, `years_to`, `order_by`, `duration`, `coordinator`, `partners`, `web`, `internalCode`, `annotationSK`, `annotationEN`) VALUES
(1, 2, 'VEGA', '1/0937/14 ', 'Pokrocilé metódy nelineárneho modelovania a riadenia mechatronických systémov ', 'Advanced methods for nonlinear modeling and control of mechatronic systems ', '2014', '2017', 2014, 2017, '2017-12-31', '2014-2017 ', 'prof. Ing. Mikuláš Huba, PhD. ', '', '', '1425', 'Projekt sa zameriava na rozvoj metód nelineárneho riadenia a ich aplikácií. Zahrňuje metódy algebrického a diferenciálneho prístupu k návrhu nelineárnych systémov, riadenie časovo oneskorených (time delayed) systémov a systémov s obmedzeniami uvažovaných ako súčasť hybridných, autonómnych a inteligentných systémov, metódy simulácie, modelovania a automatizovaného návrhu s využitím podporných numerických a symbolických metód a programov. Venuje sa formulácii riešených problémov v rámci vnorených (embedded) systémov a PLC, spracovaniu signálov, zohľadneniu aspektov riadenia cez Internet, mobilné a rádiové siete, identifikácii a kompenzácii nelinearít, integrácii jednotlivých prístupov pri implementácii a fyzickej realizácii konkrétnych algoritmov a štruktúr riadenia. Pôjde najmä o riadenie mechatronických, robotických a ďalších systémov s dominantnými nelinearitami.', 'The project focuses on development of nonlinear control methods and their applications. It includes algebraic and differential approach to nonlinear control, control of time-delayed and constrained systems considered as a part of hybrid autonomous intelligent systems, simulations modeling and automatized design based on numeric and symbolic computer aided methods. It is dealing with formulation of solved problems within the embedded systems and PLCs, with signal processing, control via Internet, mobile and radio networks, with identification and compensation of nonlinearities, integration of particular approaches in implementing and physically accomplishing particular algorithms and structures. Thereby, one considers especially mechatronic and robotic systems and other systems with dominating nonlinear behavior.'),
(2, 2, 'VEGA', '1/0228/14 ', 'Modelovanie termohydraulických a napätostných pomerov vo vybraných komponentoch tlakovodných jadrových reaktorov ', 'Modelling of thermohydraulic and stress conditions in selected components of NPP with pressurized water reactor ', '2014', '2016', 2014, 2016, '2016-12-31', '2014-2016 ', 'doc. Ing. Vladimír Kutiš, PhD. ', '', '', '1435', 'Cieľom predkladaného projektu je tvorba matematických modelov vybraných komponentov jadrových zariadení tlakovodného jadrového reaktora ako sú palivová kazeta, aktívna zóna ako aj celý jadrový reaktor. Tieto komponenty budú analyzované z pohľadu termohydrauliky ako aj z pohľadu mechanického (napätostného) namáhania. Takto získané numerické výsledky budú konfrontované s dostupnými experimentálnymi údajmi daných zariadení, pričom cieľom má byť zvyšovanie bezpečnosti prevádzky týchto zariadení. Pri tvorbe jednotlivých matematických modelov budú použité moderné numerické metódy, ako sú Computational Fluid Dynamics (CFD) a Metóda Konečných Prvkov (MKP), ktoré sú implementované v programoch ANSYS CFX a ANSYS Multiphysics. Súčasťou predkladaného projektu bude realizácia prepojenia matematických modelov termohydrauliky a mechanického namáhania, ktoré bude realizované tak, aby jednotlivé fyzikálne domény boli priamo previazané. Výstupom projektu okrem matematických modelov budú aj vedecké a odborné články a príspevky.', 'The aim of this project is to create mathematical models of selected components of nuclear power plants like fuel assembly, the active zone as well as a nuclear reactor itself considering pressurized water reactor. These components will be analyzed in terms of thermo-hydraulics and mechanical point of view (stress loading). Obtained numerical results will be confronted with available experimental data to increase operational safety of these devices. In the process of developing the mathematical models modern numerical methods such as Computational Fluid Dynamics (CFD) and Finite Element Method (FEM) will be used. These methods are implemented in programs ANSYS CFX and ANSYS Multiphysics. The proposed project will interconnect the thermo-hydraulic and mechanical mathematical models, which will be implemented so that the individual physical domains were directly connected. The outcome of the project will be the mathematical models and also scientific and technical papers and conference contributions.'),
(3, 2, 'VEGA', '1/0453/15 ', 'Výskum stiesneného krútenia uzatvorených prierezov ', 'Research of nonuniform torsion of cross-sections ', '2015', '2017', 2015, 2017, '2017-12-31', '2015-2017 ', 'prof. Ing. Justín Murín, DrSc. ', '', '', '1479', 'Podstatou projektu je skúmanie účinkov stiesneného krútenia v nosníkoch s uzatvoreným tenkostenným prierezom numerickými metódami ako aj experimentálnym meraním na fyzikálnych modeloch. Bude vytvorený nový 3D nosníkový konečný prvok so zahrnutím stiesneného krútenia uzatvorených prierezov, kde sa uplatní deformačný účinok sekundárneho krútiaceho momentu. Matica tuhosti a hmotnosti bude zostavená pre homogénny materiál ako aj pre kompozitné nosníky s pozdĺžnou premenlivosťou materiálových vlastností.\nOdvodené vzťahy a rovnice budú implementované do počítačového programu pre elastostatickú a modálnu analýzu s uvažovaním stiesneného krútenia. Bude navrhnuté a vyrobené meracie zariadenie, ktorým sa budú verifikovať výsledky teoretických výpočtov novým konečným prvkom. Predpokladá sa, že výsledky riešenia projektu prispejú ku zmene tvrdenia normy EC 3, podľa ktorej vplyv stiesneného krútenia možno pri nosníkoch uzatvoreného prierezu zanedbať. Výsledky nášho výskumu majú za cieľ zvýšiť bezpečnosť projektovania mechanických sústav.', 'The project aim is to examine the effects of non-uniform torsion in thin-walled beams with closed cross-section by numerical methods and experimental measurements on physical models. A 3D beam finite element will be created including the non-uniform torsion with the secondary torsion moment deformation effect. The stiffness and mass matrix will be prepared for a homogeneous material as well as for composite beams with longitudinal variation of material properties. Derived relations and equations will be implemented in the computer programs for elastic-static and modal analyses. Measurement equipment will be designed, by which the results of theoretical calculations by the new finite elements will be verified. It is expected that the results of the project will contribute to review the arguments of the Eurocode 3, according to which the effect of non-uniform torsion can be neglected in the closed cross-section beams. The results of the project are intended to enhance the safety of the beam structures design.'),
(4, 4, 'KEGA', '035STU-4/2014 ', 'Návrh virtuálneho laboratória pre implementáciu pokrocilých metodík výucby v novom študijnom programe Elektromobilita ', 'Development of virtual laboratory for implementation of advanced methods of teaching in the new study program Electromobility ', '2014', '2016', 2014, 2016, '2016-12-31', '2014-2016 ', 'prof. Ing. Viktor Ferencey, PhD. ', '', '', '1709', 'Projekt je zameraný na vybudovanie moderného špecializovaného virtuálneho laboratória pre pripravovaný študijný program Elektromobilita. V projekte sú navrhnuté pokročilé metódy výučby, ktoré integrujú tvorivú implementáciu teoretických poznatkov priamo do virtuálneho modelovania a simulovania mechatronických systémov v inteligentných vozidlách s elektrickým pohonom, t.j. elektromobiloch.\nPre podporu špecializovaného vzdelávania a novú metodológiu v študijnom programe Elektromobilita bude v projekte spracovaná nová moderná študijná literatúra a vybudované Špecializované virtuálne laboratórium s inovatívnym vybavením pre teoretickú i praktickú výučbu predmetov v tomto študijnom programe. Všetky predmety programu Elektromobilita sú zamerané na virtuálne prototypovanie smart mechatronických systémov používaných v elektromobiloch s náväznosťou na nové systémy pohonu dopravných prostriedkov s využitím virtuálneho prototypovania.\nSúčasťou projektu bude spracovanie študijných materiálov, vedeckých monografií, tvorba inovatívnej web stránky, publikovanie v odborných časopisoch a účasť na vedeckých konferenciách. Špecializované virtuálne laboratórium bude vybavené mechatronickými učebnými modulmi pre výučbu a štúdium sofistikovaných technológií.', 'The project aim it to build a modern specialized virtual laboratory for prepared study program Electromobility. In this project, advanced teaching methods are proposed that integrate theoretical knowledge into practical application directly into mechatronic systems in vehicles with electric drive (electric vehicles). To promote specialized training and a new methodology in the study program Electromobility, the project will support processing of a new modern study literature and creating a dedicated virtual laboratory with innovative facilities for theoretical and practical training courses in this program of study. These courses aim at smart mechatronic systems used in electromobility systems with links to the new drive systems of vehicles using virtual prototyping. The project includes new study materials processing, writing scientific monographs, creating innovative websites, publications in peer-reviewed journals and participation in scientific conferences. Dedicated virtual laboratory will be equipped with educational mechatronic modules for teaching and learning sophisticated technology.'),
(5, 4, 'KEGA', '032STU-4/2013 ', 'Online laboratórium pre výucbu predmetov automatického riadenia ', 'Online laboratory for teaching automation control subjects ', '1.1.2013', '31.12.2015', 2013, 2015, '2015-12-31', '1.1.2013-31.12.2015 ', 'doc. Ing. Katarína Žáková, PhD. ', '', '', '1719', 'Projekt sa zameriava na tvorbu interaktívnych znovupoužiteľných vzdelávacích objektov pre zvolené segmenty teórie automatického riadenia, na budovanie širšej škály experimentov ilustrujúcich aplikáciu študovaných teoretických prístupov na riešenie praktických problémov, ktoré umožňujú a podporujú nadobúdanie vedomostí, zručností, návykov a postojov v kvázi-autentickom prostredí.\nProjekt má za cieľ podporovať využitie nielen proprietárnych, ale aj open technológií, ktoré prinášajú viaceré výhody v oblasti šírenia výsledkov a nesporne aj po finančnej stránke. Snahou je uľahčiť prístup k laboratórnym experimentom v rámci rôznych foriem vzdelávania (denných, dištančných, resp. elektronických foriem).', 'The project is focussed on development of interactive reusable learning objects for chosen segments of automatic control, on building broader spectrum of experiments illustrating application of studied\ntheoretical approaches onto practical problems enabling and supporting acquisition of knowledge, skills, habits and attitudes in an quasi-authentic environment.\nThe project is going to support the use of not only proprietary but also open technologies that bring various advantages in the area of results dissemination and from the financial point of view as well. Our aim is to facilitate approach to laboratory experiments for students in daily or distance form of education.'),
(6, 4, 'KEGA', '030STU-4/2015 ', 'Multimediálna podpora vzdelávania v mechatronike ', 'Multimedial education in mechatronics ', '2015', '2017', 2015, 2017, '2017-12-31', '2015-2017 ', 'doc. Ing. Danica Rosinová, PhD. ', '', 'http://uamt.fei.stuba.sk/KEGA_MM/', '1723', 'Svetovým trendom v oblasti modernej a bezbariérovej výučby sú jej interaktívne formy na báze internetu, videa, audiovizuálnych pomôcok a vzdialených laboratórií (on-line vzdelávanie), ktoré sa uplatňujú nielen v dištančnom vzdelávaní, ale aj v prezenčnej forme vzdelávanie s podporou nových technológií (technology augmented classroom teaching). Popri slide-show prezentáciách a edukačných miniaplikáciách (dynamické web stránky, flash animácie, Java Applets a pod.) preferujú svetové výskumné univerzity vývoj a tvorbu edukačných videí, ktorých cieľovou skupinou sú poslucháči konkrétneho predmetu (kurzu). Edukačné videá sú voľne dostupné a umožňujú študentom sledovať výklad danej problematiky kdekoľvek a kedykoľvek. Návrh a realizácia zrozumiteľného a zaujímavo podaného edukačného videa z technickej oblasti je komplexná úloha, ktorá si vyžaduje synergiu odborných, pedagogických a umeleckých kvalít jeho tvorcov. Projekt je zameraný na multimediálnu podporu vzdelávania v oblasti mechatroniky, s dôrazom na poznatky z aplikovanej informatiky, automatizácie a príbuzných vedných disciplín. Cieľom projektu je vybudovanie multimediálneho laboratória na tvorbu kvalitných edukačných videomateriálov pre prezenčnú aj dištančnú formu univerzitného vzdelávania v oblasti mechatroniky a vytvorenie a otestovanie viacerých modulov takýchto materiálov. Výstupy projektu budú ďalej využiteľné pre účely vzdelávania odborníkov z praxe vrámci celoživotného vzdelávania, a tiež popularizácie mechatroniky a automatizácie u širokej verejnosti a žiakov stredných škôl - potenciálnych študentov vysokých škôl technického zamerania.', 'Presently, interactive education forms based on exploitation of Internet, video, audiovisual aids and remote laboratories (on-line education) are world trends in modern and barrier-free education;\nit is applied not only in distance education but in the attendance teaching as technology augmented classroom teaching. Along with slide-shows and educational miniapplications (dynamic websites,\nflash animations, Java Applets etc.) research universities usually prefer to develop their own education videos targeted to the audience in a single course. Education videos are freely available and enable the students to follow the explanatory discourse on the subject topic anytime and anywhere. Design and realization of a comprehensible and interesting educational video on a technological field is a quite complex task requiring synergy of technical, educational and artistic qualities of its creators. The project deals with the multimedia support of education in mechatronics engineering, with the focus on applied informatics, automation and related fields. The objective of the project is to build a multimedia laboratory for creating high-quality educational videomaterial for both distance and attendance education in mechatronics engineering. Project outcomes will be further employed in life-long education of practitioners, and for popularization of mechatronics and automation among the public and potential university students of technology.'),
(7, 4, 'KEGA', '011STU-4/2015 ', 'Elektronické pedagogicko-experimentálne laboratóriá mechatroniky ', 'Electronic educational-experimental laboratories of Mechatronics ', '2015', '2017', 2015, 2017, '2017-12-31', '2015-2017 ', 'doc. Ing. Peter Drahoš, PhD. ', '', 'http://uamt.fei.stuba.sk/kega/', '1724', 'Projekt sa zaoberá vytvorením modernej vedomostnej a experimentálnej základne pre výučbu mechatroniky s dôrazom na jej elektronické súčasti. Vzhľadom na to, že mechatronika integruje viaceré oblasti poznania a ich spojením vytvára synergický efekt, budú v rámci projektu budú vypracované nové metódy a formy vo výučbe, ktoré študentom umožnia získať nové poznatky s praktickou skúsenosťou s využívaním moderných elektronických prvkov a systémov, ktoré tvoria neoddeliteľnú súčasť komplexných mechatronických systémov v oblasti výrobkov spotrebnej elektroniky, energetiky, automobilovej techniky a v zdravotníctve.\nPodnetnou výzvou pre podanie projektu bol vznik nových študijných programoch""Automobilová mechatronika"" (Bc. program) a ""Aplikovaná mechatronika a elektromobilita"" (Ing. program). Pre tieto študijné programy budú vytvorené elektronické učebné texty pre 7 predmetov.\nZa účelom ďalšieho zvyšovania kvality výučby a výskumu sa plánuje v rámci v rámci riešenia projektu vytvoriť 5 nových experimentálnych pracovísk podľa najnovších trendov v elektronike, snímacej technike a riadiacich systémoch, ktoré budú mať viacúčelové využitie v priamej pedagogike, v individuálnych a tímových študentských projektoch ako aj pri výskumnej a vývojovej činnosti ústavu.\nCieľom projektu je zvýšiť odborné kompetencie študentov, učiteľov a výskumných pracovníkov a všetkých zúčastnených v týchto oblastiach: moderné senzory a MEMS, aktuátory na báze smart materiálov, elektrické trakčné pohony, mikroradiče a DSP pre vstavané riadiace systémy a spracovanie signálov, návrh riadiacich algoritmov a ich programovanie, elektronika a integrované obvody (ASICs) pre mechatroniku. Ďalším dôležitým sub-cieľom riešenej problematiky je získať široké kompetencie v komunikačných systémoch pre rôzne aplikačné oblasti mechatronických systémov najmä v automobilovom priemysle.\nNavrhovaný projekt bude podporovaný prostredníctvom moderných audiovizuálnych systémov, prostredníctvom web stránky a videí s multimediálnym spracovaním.', 'The project deals with the creation of modern knowledge and experimental basis for education in Mechatronics Engineering with the emphasis on electronic components. Due to the fact that mechatronics integrates several fields of knowledge and their junction yields a synergy effect, new methods and forms of eduation will be elaborated within the project allowing students to acquire new knowledge combined with practical experience in using modern electronic components and systems; such systems are integral parts of complex pervasive mechatronic systems (in consumer electronics, energy and automotive industries, healthcare). Inspiration for elaboration of the proposed project was launching of new study programs ""Automotive Mechatronics"" (Bachelor degree), and ""Applied Mechatronics and Electromobility"" (Master degree). For these study programs electronic textbooks for 7 subjects will be created. To further increase quality of education and research, 5 new experimental workplaces are planned to be created within the project to according to the latest development trends electronics, sensing technology and control systems having multi-purpose exploitation in direct teaching, individual and team projects as well as in research and development activities of the Institute. The objective of the project is to increase professional competences of students, teachers and researchers, and all involved in the areas: advanced sensors and MEMS, smart materials based actuators, electric traction motors, microcontrollers and digital signal processors (DSP´s) for embedded control systems and signal processing, design of control algorithms and their programming, electronics and integrated circuits (ASICs) for mechatronics. Another important sub-objective is to acquire wide competences in communication systems for various application areas of mechatronic systems, in particular in automotive industry. Modern audiovisual systems, web pages and multimedia processed videos will be widely used to support project results.'),
(8, 3, 'APVV', 'APVV-0246-12 ', 'Pokročilé metódy modelovania a simulácie SMART mechatronických systémov ', 'Advanced Methods and Simulations of SMART Mechatronic Systems ', '1.10.2013', '30.9.2016', 2013, 2016, '2016-9-30', '1.10.2013-30.9.2016 ', 'prof. Ing. Justín Murín, DrSc. ', '', '', 'AK14', 'V prvej fáze riešenia projektu bude kladený dôraz na materiálové, technické a prístrojové zabezpečenie experimentálnych častí, ktoré budú v projekte riešené. V tejto fáze takisto budú odvodené MKP rovnice pre 3D-FGM nosníky ako aj multifyzikálne modely pre SMA. Súčasťou prvej fázy riešenia projektu bude taktiež začatá príprava fyzikálnych experimentov za účelom verifikácie matematických modelov FGM a SMA systémov. V nasledovnom období riešenia projektu bude vykonaná verifikácia matematických modelov na vybraných experimentálnych vzorkách, ktoré boli dôsledne experimentálne analyzované z hľadiska materiálového zloženia. Výsledky experimentálnych meraní na SMA aktuátore budú využité v nasledovnom období riešenia projektu pri návrhu a realizácii alternatívneho spôsobu uchytenia SMA aktuátora. Bude nasledovať vytvorenie nelineárneho modelu aktuátora SMA a návrhu nových metód syntézy zameraných na riadenie polohy a potlačenie dominantných porúch. V tomto období budú súčasne prebiehať výskumné práce na teoretickom odvodení MKP rovníc pre FGM škrupinu a jej spojenia s 3D-FGM nosníkovým prvkom do kombinovaného nosníkovo-škrupinového MEMSu. V záverečnej fáze projektu bude kladený dôraz jednak na verifikáciu odvodených MKP rovníc pre nosníkovo-škrupinový MEMS pomocou fyzikálneho experimentu ako aj na riadenie SMA aktuátora konvenčnými a inteligentnými metódami riadenia.', 'In the first phase, attention will be given to the material, technical and equipment set-up required for the first set experiments. At the same time, the FGM-beam FEM equations will be derived and SMA models designed. In addition, the first sets of experiments will be used for the verification of numerical models of 3D-FGM and SMA systems. A complex verification of numerical models will take place on selected samples whose chemistry has been consistently analyzed. Results of SMA actuator measurements will be used in the consecutive stages of the project in the design and application of alternative anchoring for SMA actuators. Next the nonlinear model of SMA actuator and new methods of synthesis focused on position control and error elimination will be proposed. This research will take place in parallel with the theoretical analysis and FEM equations derivation of FGM shells. In the final stage, emphasis will be given to both the verification of the derived FGM beam-shell equations by real sample measurements and the control of the SMA actuator by conventional and intelligent methods.'),
(9, 3, 'APVV', 'APVV-0343-12 ', 'Počítačová podpora návrhu robustných nelineárnych regulátorov ', 'Computer aided robust nonlinear control design ', '1.10.2013', '31.3.2017', 2013, 2017, '2017-3-31', '1.10.2013-31.3.2017 ', 'prof. Ing. Mikuláš Huba, PhD. ', '', '', 'AK29', 'Projekt sa zaoberá vypracovaním podporného počítačového systému na návrh robustných nelineárnych regulátorov s obmedzeniami vo verzii pre Matlab/Simulink a web a vytvorením integrovaného elektronického prostredia v LMS Moodle, ktoré ho spája s webovou stránkou projektu, s elearningovými modulmi a s prístupom k vzdialeným experimentom umožňujúcim jeho overenie online. Systém je založený na novej metóde návrhu regulátorov vychádzajúcej s obmedzovania odchýlok od požadovaných tvarov vstupných a výstupných, resp. stavových veličín. Táto integruje výsledky viacerých doteraz izolovaných prístupov k návrhu regulátorov - tradičnú teóriu PID regulátorov, moderný stavový prístup s teóriou pozorovateľov, časovo optimálne riadenie, nelineárne systémy a riadenie systémov s veľkým dopravným oneskorením a robustný návrh regulátorov. Vyvíjaný systém bude vhodný pre širokú triedu neurčitých a nelineárnych objektov, ktoré predstavujú väčšinu bežných aplikácií v praxi. Systém bude pozostávať z centrálnej pracovnej stanice umožňujúcej dostatočne rýchle generovanie tzv. portrétu správania riadeného objektu s uvažovaným typom regulátora, z úložiska vytvorených portrétov správania a z grafických staníc, ktoré umožnia na základe špecifikácie neurčitých parametrov riadeného objektu a zadaných kvalitatívnych požiadaviek na riadené procesy určiť optimálne nastavenie regulátora zaručujúce pre zadané požiadavky dosiahnutie najvyššej možnej dynamiky prechodových dejov aj pri zohľadnení neurčitostí.', 'The project deals with development and introduction into practice of the computer aided system for design of robust constrained nonlinear control (in versions for Matlab/Simulink and web) and of the integrated electronic environment in LMS Moodle interconnecting the system with the project web page, with the elearning modules and with access to remote experiments enabling its online verification. The system will be based on a new robust control method based on constraining deviations from required shapes of the input, output, or state variables. This is holistically integrating several up to now isolated control design approaches - the traditional PID control, modern state & disturbance observer approach, minimum time control, nonlinear control, control of systems with long dead time and robust control. The developed system is intended for a broad class of uncertain and nonlinear plants that represent a majority of all applications in practice. The system will consist of a central work station enabling a sufficiently fast generation of the so called performance portrait of given plant with a considered type of control, from a repository of generated performance portraits and from graphical terminals enabling by means of specifying parameters of given plant and the required shape-related performance measures to determine the optimal controller tuning guaranteeing the fastest possible transients responses in the control loop under consideration of the given uncertainties.'),
(10, 3, 'APVV', 'APVV-0772-12 ', 'Moderné metódy riadenia s využitím FPGA štruktúr ', 'Advanced control methods based on FPGA structures ', '1.10.2013', '30.9.2017', 2013, 2017, '2017-9-30', '1.10.2013-30.9.2017 ', 'doc. Ing. Alena Kozáková, PhD. ', '', '', 'AK39', 'Projekt rieši aktuálne otázky výskumu a vývoja moderných metód riadenia s využitím hardvérových realizácií konvenčných (PID) ako aj moderných (optimálne, robustné, prediktívne) algoritmov riadenia pre procesy s rýchlou dynamikou. V súčasnosti dominujú vo výskume a implementácii moderných riadiacich systémov tieto smery: riešenia na báze mikroprocesorov (softvérový prístup), jednoúčelové riešenia ASIC a riešenia na báze programovateľných hradlových polí (Field Programmable Gate Arrays, FPGA), ktoré predstavujú konfigurovateľné obvody vysokého stupňa integrácie (VLSI) schopné integrovať rôzne logické a riadiace funkcie. Hardvérové implementácie algoritmov riadenia sú v porovnaní so softvérovými realizáciami vo všeobecnosti o niekoľko rádov rýchlejšie, pretože spracovanie v nich prebieha paralelne, navyše sú kompaktnejšie a vo všeobecnosti lacnejšie. Hlavným cieľom projektu je výskum a vývoj algoritmov na báze FPGA štruktúr, ktorý bude skúmaný na vývojových FPGA systémoch a verifikovaný na fyzikálnych laboratórnych modeloch s rýchlou dynamikou.', 'The project deals with research and development of advanced control methods based on HW implementation of conventional (PID) as well as modern optimal, robust and predictive algorithms applicable in control of plants with fast dynamics. Predominating approaches in the research of modern control systems implementation are microprocessor-based solutions (software approach), ASIC (dedicated approach) and the FPGA based approach. Field Programmable Gate Arrays (FPGA) are configurable circuits of very-large-scale-integration (VLSI) able to integrate various logical and control functions. In general, HW implementations of control algorithms are faster by several orders compared with SW implementations due to parallel processing of information; moreover they are more compact and also less expensive. The main objective of the project is research and development of FPGA-based control algorithms. Their research and development will be studied on available FPGA development kits and verified on laboratory plants with fast dynamics.'),
(11, 3, 'APVV', 'APVV-14-0613 ', 'Širokopásmový MEMS detektor terahertzového žiarenia ', 'Broadband MEMS detector of terahertz radiaton ', '2015', '2018', 2015, 2018, '2018-12-31', '2015 - 2018 ', 'doc. Ing. Vladimír Kutiš, PhD. ', '', '', '', '', 'The project is aimed on research and development of new types of broadband detectors for terahertz frequency range. This new type of detector is designed in a concept of micro-electro-mechanical system and uses the bolometric sensing principle. The design construction of the detector consists of a microbolometric sensing device coupled to a broadband antenna. Thermal conversion of the incident THz radiation takes place on a thin polyimide membrane which enables (a) to achieve high thermal conversion efficiency and (b) to design detectors with balanced amplitude characteristics even at high frequency range. The proposed MEMS detector concept will be optimized by a sophisticated process of modeling and simulation in direct mutual iteration with experimental analysis of functionality and detection capability. The completion of the project will be given by the developed state-of-the-art methodology of characterization, broadband THz detection and simulation of the MEMS detector device applicable in the research and education.'),
(12, 1, 'Program EHP Slovakia – Mobility projects among universities ', 'SK06-II-01-004 ', 'Podpora medzinárodnej mobility medzi STU Bratislava, NTNU Trondheim a Universität Liechtenstein ', 'Support of international mobilites between STU Bratislava, NTNU Trondheim, and Universität Liechtenstein ', '2.6.2015', '30.9.2016', 2015, 2016, '2016-9-30', '2.6.2015 - 30.9.2016 ', 'zodpovedný za ÚAMT - prof. Ing. Mikuláš Huba, PhD. ', 'Norwegian University of Science and Technology Trondheim (prof. Skogestad, prof. Johansen, prof. Hovd)|Universität Liechtenstein, Liechtenstein (prof. Droege)', '', '', 'Projekt rieši aktuálne otázky výskumu a vývoja moderných metód riadenia s využitím hardvérových realizácií konvenčných (PID) ako aj moderných (optimálne, robustné, prediktívne) algoritmov riadenia pre procesy s rýchlou dynamikou. V súčasnosti dominujú vo výskume a implementácii moderných riadiacich systémov tieto smery: riešenia na báze mikroprocesorov (softvérový prístup), jednoúčelové riešenia ASIC a riešenia na báze programovateľných hradlových polí (Field Programmable Gate Arrays, FPGA), ktoré predstavujú konfigurovateľné obvody vysokého stupňa integrácie (VLSI) schopné integrovať rôzne logické a riadiace funkcie. Hardvérové implementácie algoritmov riadenia sú v porovnaní so softvérovými realizáciami vo všeobecnosti o niekoľko rádov rýchlejšie, pretože spracovanie v nich prebieha paralelne, navyše sú kompaktnejšie a vo všeobecnosti lacnejšie. Hlavným cieľom projektu je výskum a vývoj algoritmov na báze FPGA štruktúr, ktorý bude skúmaný na vývojových FPGA systémoch a verifikovaný na fyzikálnych laboratórnych modeloch s rýchlou dynamikou.', 'The aim of the project is to support international mobility of students, PhD students, and staff members of four participating faculties of STU in Bratislava with partners from NTNU Trondheim and Universität Liechtenstein. It will initiate academic cooperation between the University of Liechtenstein and STU Bratislava in construction, architecture, and space planning, focusing on the use of alternative energy sources in operation of buildings, including computer-aided simulations of energy needs and internal environment, and spatial planning of rural settlements as well. The project also contributes to further strengthening of already existing cooperation between NTNU Trondheim and faculties of STU in Bratislava in the field of advanced methods of automatic control and to progress of inter-faculty cooperation at STU in Bratislava.'),
(13, 5, 'Iné domáce projekty', 'TB ', 'Softvérové riadenie smerovej dynamiky vozidla UGV 6x6 ', 'Softvérové riadenie smerovej dynamiky vozidla UGV 6x6 ', '2015', '2015', 2015, 2015, '2015-12-31', '2015', 'Ing. Martin Bugár, PhD. ', '', '', '7506', '', ''),
(14, 5, 'Iné domáce projekty', 'VW ', 'Predlžovanie životnosti akumulátorového systému ', 'Predlžovanie životnosti akumulátorového systému ', '2015', '2015', 2015, 2015, '2015-12-31', '2015', 'Ing. Martin Bugár, PhD. ', '', '', '7509', '', ''),
(15, 5, 'Iné domáce projekty', 'MV ', 'REST platforma pre online riadenie experimentov ', 'REST Platform for Online Control of Experiments ', '2015', '2015', 2015, 2015, '2015-12-31', '2015', 'Ing. Miroslav Gula ', '', '', '1361', 'Tento projekt je súčasťou rozsiahlejšieho cieľa o vytvorenie univerzálneho protokolu pre vzdialené riadenie reálnych sústav a tiež balíka softvérových nástrojov na jeho implementáciu. Hlavným cieľom celého úsilia je zjednodušiť a urýchliť budovanie modulárnych online laboratórií.\nÚlohami projektu sú návrh a vytvorenie nástroaj pre vzdialený prístup k softvéru Scilab, zavŕšenie implementácie podobného nástroja určeného pre softvérový balík Matlab/Simulink, a návrh a čiastočná implementácia mechatronického systému, ktorý bude v budúcnosti slúžiť na demonštráciu spomenutých nástrojov a následne ako učebná pomôcka.', 'The project is a part of an extensive endeavor to create universal protocol for remote control of real plants, and a suite of software tools to implement this protocol. The main objective of this whole endeavor is to simplify and accelerate implementation of modular online laboratories. Tasks of this project include design and implementation of a software tool for remote access to Scilab, completion of implementation of a similar tool for Matlab/Simulink, and design and partial implementation of a mechatronic system which will serve for demonstration of mentioned tools and later on as teaching aid.');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `typ_nepritomnosti`
--

CREATE TABLE `typ_nepritomnosti` (
  `id` int(11) NOT NULL,
  `typ` varchar(20) NOT NULL,
  `skratka` varchar(5) NOT NULL,
  `farba` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Sťahujem dáta pre tabuľku `typ_nepritomnosti`
--

INSERT INTO `typ_nepritomnosti` (`id`, `typ`, `skratka`, `farba`) VALUES
(1, 'Práce neschopný', 'PN', '#d9534f'),
(2, 'Ošetrovanie č rodiny', 'OČR', 'darkgrey'),
(3, 'Služobná cesta', 'SC', 'gold'),
(4, 'Dovolenka', 'D', 'blue'),
(5, 'Plánovaná dovolenka', 'PD', 'darkblue');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `videa`
--

CREATE TABLE `videa` (
  `id` int(11) NOT NULL,
  `meno` varchar(95) DEFAULT NULL,
  `url` varchar(11) DEFAULT NULL,
  `typ` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Sťahujem dáta pre tabuľku `videa`
--

INSERT INTO `videa` (`id`, `meno`, `url`, `typ`) VALUES
(1, 'Vzdialené experimenty - podpora pre vzdelávanie', 'Z0zBwR_MKOI', 'labák'),
(2, 'Multimédiá a telematika pre mobilné platformy - voliteľný predmet v inžinierskom štúdiu FEI STU', 'NKZmJB0PW3k', 'predmet'),
(3, 'Študuj mechatroniku a budeš úspešný!', 'vCYq4JspSCI', 'propagácia'),
(4, 'Mechatronické kresliace rameno mScara - Makeblock mDrawBot kit ', 'qmijnl8jwaw', 'zariadenie'),
(5, 'Riadenie modelu výrobného systému cez PLC', 'ymqYxRYt5sY', 'zariadenie'),
(6, 'Inžinierska informatika v mechatronike - Ing. ŠP Aplikovaná mechatronika a elektromobilita', 'CLwEjKN9ixg', 'propagácia'),
(7, 'Ústav automobilovej mechatroniky FEI STU ', 'IiNXYgbOKxw', 'propagácia');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `zamestnanci`
--

CREATE TABLE `zamestnanci` (
  `id` int(11) NOT NULL,
  `name` varchar(60) COLLATE utf8_slovak_ci DEFAULT NULL,
  `surname` varchar(60) COLLATE utf8_slovak_ci DEFAULT NULL,
  `title1` varchar(60) COLLATE utf8_slovak_ci DEFAULT NULL,
  `title2` varchar(60) COLLATE utf8_slovak_ci DEFAULT NULL,
  `ldapLogin` varchar(60) COLLATE utf8_slovak_ci DEFAULT NULL,
  `photo` varchar(60) COLLATE utf8_slovak_ci DEFAULT NULL,
  `room` varchar(60) COLLATE utf8_slovak_ci DEFAULT NULL,
  `phone` varchar(60) COLLATE utf8_slovak_ci DEFAULT NULL,
  `department` varchar(60) COLLATE utf8_slovak_ci DEFAULT NULL,
  `staffRole` varchar(60) COLLATE utf8_slovak_ci DEFAULT NULL,
  `function` varchar(100) COLLATE utf8_slovak_ci DEFAULT NULL,
  `url` varchar(100) COLLATE utf8_slovak_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovak_ci;

--
-- Sťahujem dáta pre tabuľku `zamestnanci`
--

INSERT INTO `zamestnanci` (`id`, `name`, `surname`, `title1`, `title2`, `ldapLogin`, `photo`, `room`, `phone`, `department`, `staffRole`, `function`, `url`) VALUES
(1, 'Vladislav', 'Bača', 'Ing.', '', '', 'baca.jpg', 'T005', '264', 'OEMP', 'doktorand', '', 'http://is.stuba.sk../lide/clovek.pl?id=5559'),
(2, 'Peter', 'Balko', 'Ing.', '', '', '', 'D 102', '395', 'OIKR', 'doktorand', '', 'http://is.stuba.sk../lide/clovek.pl?id=56112'),
(3, 'Richard', 'Balogh', 'Ing.', ' PhD.', '', 'balogh.jpg', 'D 110', '411', 'OEMP', 'teacher', 'zástupca vedúceho oddelenia', 'http://is.stuba.sk../lide/clovek.pl?id=3706'),
(4, 'Igor', 'Bélai', 'Ing.', ' PhD.', '', '', 'D 126', '478', 'OEMP', 'teacher', '', 'http://is.stuba.sk../lide/clovek.pl?id=4947'),
(5, 'Katarína', 'Beringerová', '', '', '', '', 'A 705', '672', 'AHU', 'teacher', '', 'http://is.stuba.sk../lide/clovek.pl?id=2049'),
(6, 'Pavol', 'Bisták', 'Ing.', ' PhD.', 'bistak', 'bistak.jpg', 'D 120', '695', 'OEAP', 'teacher', '', 'http://is.stuba.sk../lide/clovek.pl?id=3708'),
(7, 'Dmitrii', 'Borkin', 'Ing.', '', '', '', 'D 102', '395', 'OIKR', 'doktorand', '', 'http://is.stuba.sk../lide/clovek.pl?id=87894'),
(8, 'Martin', 'Bugár', 'Ing.', ' PhD.', '', '', 'A 708', '579', 'OEAP', 'teacher', '', 'http://is.stuba.sk../lide/clovek.pl?id=14157'),
(9, 'Ján', 'Cigánek', 'Ing.', ' PhD.', '', '', 'D 104', '686', 'OIKR', 'teacher', '', 'http://is.stuba.sk../lide/clovek.pl?id=10327'),
(10, 'Peter', 'Drahoš', 'doc. Ing.', ' PhD.', '', '', 'D 118', '669', 'OEMP', 'teacher', '', 'http://is.stuba.sk../lide/clovek.pl?id=3709'),
(11, 'František', 'Erdödy', '', '', '', 'erdody.jpg', 'A S39', '818', 'AHU', 'teacher', '', 'http://is.stuba.sk../lide/clovek.pl?id=2156'),
(12, 'Viktor', 'Ferencey', 'prof. Ing.', ' PhD.', '', 'ferencey.jpg', 'A 802', '438', 'OEAP', 'teacher', 'zástupca vedúceho oddelenia', 'http://is.stuba.sk../lide/clovek.pl?id=2209'),
(13, 'Peter', 'Fuchs', 'doc. Ing.', ' PhD.', '', '', 'B S05', '826', 'OEMP', 'researcher', '', 'http://is.stuba.sk../lide/clovek.pl?id=1898'),
(14, 'Gabriel', 'Gálik', 'Ing.', '', '', '', 'A 706', '559', 'OAMM', 'researcher', '', 'http://is.stuba.sk../lide/clovek.pl?id=55772'),
(15, 'Vladimír', 'Goga', 'doc. Ing.', ' PhD.', '', '', 'A 702', '687', 'OAMM', 'teacher', '', 'http://is.stuba.sk../lide/clovek.pl?id=10291'),
(16, 'Miroslav', 'Gula', 'Ing.', '', 'xgulam', 'gula.jpg', 'D 103', '628', 'OIKR', 'doktorand', '', 'http://is.stuba.sk../lide/clovek.pl?id=17491'),
(17, 'Oto', 'Haffner', 'Ing.', ' PhD.', '', 'haffner.jpg', 'D 125', '315', 'OIKR ', 'teacher', '', 'http://is.stuba.sk../lide/clovek.pl?id=56107'),
(18, 'Juraj', 'Hrabovský', 'Ing.', ' PhD.', '', '', 'A 706', '559', 'OAMM', 'teacher', '', 'http://is.stuba.sk../lide/clovek.pl?id=27795'),
(19, 'Mikuláš', 'Huba', 'prof. Ing.', ' PhD.', '', 'huba.jpg', 'D 112', '771', 'OEAP', 'teacher', 'riaditeľ ústavu; vedúci oddelenia', 'http://is.stuba.sk../lide/clovek.pl?id=2076'),
(20, 'Mária', 'Hypiusová', 'Ing.', ' PhD.', '', '', 'D 122', '193', 'OIKR', 'teacher', '', 'http://is.stuba.sk../lide/clovek.pl?id=4949'),
(21, 'Štefan', 'Chamraz', 'Ing.', ' PhD.', '', '', 'D 107', '848', 'OEMP', 'teacher', '', 'http://is.stuba.sk../lide/clovek.pl?id=3711'),
(22, 'Jakub', 'Jakubec', 'Ing.', ' PhD.', '', '', 'A 707', '452', 'OAMM ', 'researcher', '', 'http://is.stuba.sk../lide/clovek.pl?id=36067'),
(23, 'Igor', 'Jakubička', 'Ing.', '', '', 'jakubicka.jpg', 'T005', '264', 'OEMP', 'doktorand', '', ''),
(24, 'Katarína', 'Kermietová', '', '', '', '', 'D 116', '598', 'AHU', 'teacher', 'zástupca vedúceho oddelenia', 'http://is.stuba.sk../lide/clovek.pl?id=45169'),
(25, 'Ivan', 'Klimo', 'Ing.', '', '', '', 'D 101', '509', 'OEMP', 'doktorand', '', 'http://is.stuba.sk../lide/clovek.pl?id=8561'),
(26, 'Michal', 'Kocúr', 'Ing.', ' PhD.', 'xkocurm2', 'kocur.jpg', 'D 104', '686', 'OIKR ', 'teacher', '', 'http://is.stuba.sk../lide/clovek.pl?id=55800'),
(27, 'Štefan', 'Kozák', 'prof. Ing.', ' PhD.', '', 'kozak.jpg', 'D 115', '281', 'OEMP', 'teacher', 'zástupca riaditeľa ústavu pre rozvoj ústavu; vedúci oddelenia', 'http://is.stuba.sk../lide/clovek.pl?id=1954'),
(28, 'Alena', 'Kozáková', 'doc. Ing.', ' PhD.', '', '', 'D 111', '563', 'OIKR', 'teacher', '', 'http://is.stuba.sk../lide/clovek.pl?id=4939'),
(29, 'Erik', 'Kučera', 'Ing.', ' PhD.', '', '', 'D 125', '315', 'OIKR ', 'teacher', '', 'http://is.stuba.sk../lide/clovek.pl?id=55901'),
(30, 'Vladimír', 'Kutiš', 'doc. Ing.', ' PhD.', '', 'kutis.jpg', 'A 701', '562', 'OAMM ', 'teacher', 'zástupca vedúceho oddelenia', 'http://is.stuba.sk../lide/clovek.pl?id=1879'),
(31, 'Alek', 'Lichtman', 'Ing.', '', '', '', 'D 101', '509', 'OEMP', 'doktorand', '', 'http://is.stuba.sk../lide/clovek.pl?id=8565'),
(32, 'Justín', 'Murín', 'prof. Ing.', ' DrSc.', '', 'murin.jpg', 'A 704', '611', 'OAMM', 'teacher', 'zástupca riaditeľa ústavu pre vedeckú činnosť; vedúci oddelenia', 'http://is.stuba.sk../lide/clovek.pl?id=1830'),
(33, 'Jakub', 'Osuský', 'Ing.', ' PhD.', '', 'osusky.jpg', 'D 123', '356', 'OIKR ', 'teacher', '', 'http://is.stuba.sk../lide/clovek.pl?id=24492'),
(34, 'Tomáš', 'Paciga', 'Ing.', '', '', '', 'A 707', '452', 'OAMM', 'doktorand', '', ''),
(35, 'Juraj', 'Paulech', 'Ing.', ' PhD.', '', 'paulech.jpg', 'A 701', '562', 'OAMM', 'teacher', '', 'http://is.stuba.sk../lide/clovek.pl?id=12290'),
(36, 'Matej', 'Rábek', 'Ing.', '', 'xrabek', 'rabek.jpg', 'D 103', '628', 'OIKR', 'doktorand', '', 'http://is.stuba.sk../lide/clovek.pl?id=8855'),
(37, 'Danica', 'Rosinová', 'doc. Ing.', ' PhD.', '', 'rosinova.jpg', 'D 111', '563', 'OIKR', 'teacher', 'vedúci oddelenia', 'http://is.stuba.sk../lide/clovek.pl?id=4938'),
(38, 'Tibor', 'Sedlár', 'Ing. ', '', '', '', 'A 803', '399', 'OAMM', 'teacher', '', 'http://is.stuba.sk../lide/clovek.pl?id=48475'),
(39, 'Erich', 'Stark', 'Ing.', '', '', 'stark.jpg', 'C 014', '', 'OIKR', 'doktorand', '', 'http://is.stuba.sk../lide/clovek.pl?id=6041'),
(40, 'Peter', 'Ťapák', 'Ing.', ' PhD.', '', '', 'D 121', '569', 'OEAP', 'teacher', '', 'http://is.stuba.sk../lide/clovek.pl?id=3715'),
(41, 'Katarína', 'Žáková', 'doc. Ing.', ' PhD.', 'zakova', 'zakova.jpg', 'D 119', '742', 'OIKR', 'teacher', 'zástupca riaditeľa ústavu pre pedagogickú činnosť; zástupca vedúceho oddelenia', 'http://is.stuba.sk../lide/clovek.pl?id=4948'),
(42, 'Robo', 'Filo', 'Inžinieris', '', 'xfilo', 'as', '123', '', '', '', '', ''),
(45, 'Stano', 'Hájekus', 'Fikus', '', 'xhajek', '', '', '', '', '', '', ''),
(46, 'Majo', 'Krkoška', '', '', 'xkrkoska', '', '', '', '', '', '', ''),
(47, 'Tomáš', 'Baraniak', NULL, NULL, 'xbaraniak', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(48, 'Sabinka', 'Bucsai', '', '', 'xbucsai', '', '', '', '', '', '', '');

--
-- Kľúče pre exportované tabuľky
--

--
-- Indexy pre tabuľku `galeria`
--
ALTER TABLE `galeria`
  ADD PRIMARY KEY (`id`);

--
-- Indexy pre tabuľku `galeriaSingle`
--
ALTER TABLE `galeriaSingle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_galery` (`fk_galery`);

--
-- Indexy pre tabuľku `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uid` (`uid`);

--
-- Indexy pre tabuľku `nepritomnost`
--
ALTER TABLE `nepritomnost`
  ADD PRIMARY KEY (`id`),
  ADD KEY `zamestnanec` (`zamestnanec`),
  ADD KEY `typ_nepritomnosti` (`typ_nepritomnosti`);

--
-- Indexy pre tabuľku `typ_nepritomnosti`
--
ALTER TABLE `typ_nepritomnosti`
  ADD PRIMARY KEY (`id`);

--
-- Indexy pre tabuľku `videa`
--
ALTER TABLE `videa`
  ADD PRIMARY KEY (`id`);

--
-- Indexy pre tabuľku `zamestnanci`
--
ALTER TABLE `zamestnanci`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ldapLogin` (`ldapLogin`);

--
-- AUTO_INCREMENT pre exportované tabuľky
--

--
-- AUTO_INCREMENT pre tabuľku `galeria`
--
ALTER TABLE `galeria`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pre tabuľku `galeriaSingle`
--
ALTER TABLE `galeriaSingle`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT pre tabuľku `login`
--
ALTER TABLE `login`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT pre tabuľku `nepritomnost`
--
ALTER TABLE `nepritomnost`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=531;
--
-- AUTO_INCREMENT pre tabuľku `typ_nepritomnosti`
--
ALTER TABLE `typ_nepritomnosti`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pre tabuľku `videa`
--
ALTER TABLE `videa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT pre tabuľku `zamestnanci`
--
ALTER TABLE `zamestnanci`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
