<?php
include('layout/head.php');
include('config/db_majo.php');
?>
        <div class="row">
            <div class="form-group col-xs-6">
            <input type="text" id="myInput" onkeyup="myFunction()" class="form-control" placeholder="<?php text(zam_filtruj_oddelenie);?>" >
            </div>  <div class="form-group col-xs-6">
            <input type="text" id="myInput2" onkeyup="myFunction2()" class="form-control" placeholder="<?php text(zam_filtruj_zaradenie);?>" >
            </div>
        </div>
    </div>

<?php


?>
<div class="table-responsive">
<table class="table table-hover table-striped display compact" id="myTable"><thead>
    <tr class='bg-primary'><th onclick="sortTable(0)" style='cursor:pointer;'><?php text(zam_meno);?></th><th><?php text(zam_miestnost);?></th><th><?php text(zam_klapka);?></th><th onclick="sortTable(3)" style='cursor:pointer;'><?php text(zam_oddelenie);?></th><th onclick="sortTable(4)" style='cursor:pointer;'><?php text(zam_zaradenie);?></th><th><?php text(zam_funkcia);?></th></tr>
    </thead>
    <tbody>
    <?php

        $sql = "SELECT * FROM zamestnanci;";
        $result = $conn->query($sql);
        if (($result->num_rows) > 0) {
            while ($row = $result->fetch_assoc()) {
                $room = $row['room'];
                $photo = $row['photo'];
                $url = $row['url'];
                $meno = $row['title1'] . " " . $row['name'] ." " .$row['surname'] ." " .  $row['title2'];

                echo "<tr style='cursor:pointer;' data-toggle='modal' class='inform' data-target='#myModal' data-photo='$photo' data-department=".$row['department']." data-phone=".$row['phone']." data-meno='$meno' data-room='$room' data-url='$url' ><td>" . $row['surname'] . " " . $row['name'] . " " . $row['title1'] . " " . $row['title2'] . "</td>";
                echo "<td>    $room   </td>";
                echo "<td>" . $row['phone'] . "</td>";
                echo "<td>" . $row['department'] . "</td>";
                echo "<td>" . $row['staffRole'] . "</td>";
                echo "<td>" . $row['function'] . "</td>";
                echo "</tr>";
            }
        }
        ?>
    </tbody>
</table>
</div>


    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Detail</h4>
                </div>
                <div class="modal-body">
                    <div id="textovepole"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><?php text(close)?></button>
                </div>
            </div>

        </div>
    </div>

<?php
include ('layout/foot.php');