<?php
/**
 * Created by PhpStorm.
 * User: Tavarez
 * Date: 14.3.2017
 * Time: 21:11 
 */
include('layout/head.php'); 
redirectIfNotLogged($user); 
     $userFromDb = getLoggedUserFromDb($db);
                                if(!($userFromDb->getAdmin() || $userFromDb->getHr())){    
redirectJavaScritp();   
                                }

$userFromDb = getLoggedUserFromDb($db); 
?>
<h2> <?php text("editRoles");?></h2>
  
<?php 
  $users = getLogins($db);
?> 
<form method="post" action="saveRoles.php">
<div class="table-responsive">
    <table class="dataTable table table-hover table-striped display compact">
        <thead>
        <tr>
            <th><?php text('zam_meno');?></th>
            <th>UID</th>
            <th>User</th>
            <th>HR</th>
            <th>Reporter</th>
            <th>Editor</th> 
            <th>Admin</th> 
        </tr>
        </thead> 
        <tbody>
        <?php foreach($users as $user){ ?> 
        <tr>   
            <td><?php echo $user->getMeno(); ?></td> 
              <td><?php echo $user->getUid(); ?></td>  
     <td> 
      <input value=1 type="radio" name="editUser-<?php echo $user->getUid(); ?>-user" <?php if ($user->getUser() == 1) echo "checked"; ?>> <i class="fa fa-check" aria-hidden="true"></i>
      <input value=0 type="radio" name="editUser-<?php echo $user->getUid(); ?>-user" <?php if ($user->getUser() != 1) echo "checked"; ?>> <i class="fa fa-times" aria-hidden="true"></i>
    </td>               
     <td>
      <input value=1 type="radio" name="editUser-<?php echo $user->getUid(); ?>-hr" <?php if ($user->getHr() == 1) echo "checked"; ?>> <i class="fa fa-check" aria-hidden="true"></i>
      <input value=0 type="radio" name="editUser-<?php echo $user->getUid(); ?>-hr" <?php if ($user->getHr() != 1) echo "checked"; ?>> <i class="fa fa-times" aria-hidden="true"></i>
    </td>  
    <td>
      <input value=1 type="radio" name="editUser-<?php echo $user->getUid(); ?>-reporter" <?php if ($user->getReporter() == 1) echo "checked"; ?>> <i class="fa fa-check" aria-hidden="true"></i>
      <input value=0 type="radio" name="editUser-<?php echo $user->getUid(); ?>-reporter" <?php if ($user->getReporter() != 1) echo "checked"; ?>> <i class="fa fa-times" aria-hidden="true"></i>
    </td> 
    <td>
      <input value=1 type="radio" name="editUser-<?php echo $user->getUid(); ?>-editor" <?php if ($user->getEditor() == 1) echo "checked"; ?>> <i class="fa fa-check" aria-hidden="true"></i>
      <input value=0 type="radio" name="editUser-<?php echo $user->getUid(); ?>-editor" <?php if ($user->getEditor() != 1) echo "checked"; ?>> <i class="fa fa-times" aria-hidden="true"></i>
    </td> 
    <td>
      <input value=1 type="radio" name="editUser-<?php echo $user->getUid(); ?>-admin" <?php if ($user->getAdmin() == 1) echo "checked"; ?>> <i class="fa fa-check" aria-hidden="true"></i>
      <input value=0 type="radio" name="editUser-<?php echo $user->getUid(); ?>-admin" <?php if ($user->getAdmin() != 1) echo "checked"; ?>> <i class="fa fa-times" aria-hidden="true"></i>
    </td> 
        </tr>
        <?php } ?> 
</tbody>  
</table>
</div> 
<hr>
<button type="submit" class="btn btn-primary btn-block btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i><?php text('uloz');?></button>
</form> 
<?php include('layout/foot.php'); ?>
