<?php
/**
 * Created by PhpStorm.
 * User: Tavarez
 * Date: 25.2.2017
 * Time: 20:42
 */
class Zamestnanec
{
    protected $id, $meno, $priezvisko;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getMeno()
    {
        return $this->meno;
    }

    /**
     * @return mixed
     */
    public function getPriezvisko()
    {
        return $this->priezvisko;
    }



    public function getCeleMeno() {
        return $this->priezvisko. ' ' . $this->meno ;
    }
}