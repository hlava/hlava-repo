<?php

/**
 * Created by PhpStorm.
 * User: Tavarez
 * Date: 15.5.2017
 * Time: 13:47
 */
class galerySingle
{
    protected $fk_galery, $title, $descr, $path, $path_thumbnail, $titleEn, $descrEn, $size;

    /**
     * @return mixed
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @return mixed
     */
    public function getTitleEn()
    {
        return $this->titleEn;
    }

    /**
     * @return mixed
     */
    public function getDescrEn()
    {
        return $this->descrEn;
    }

    /**
     * @return mixed
     */
    public function getFkGalerz()
    {
        return $this->fk_galerz;
    }

    public function getDescr($lang)
    {
        if ($lang == "sk") {
            return $this->descr;
        } else {
            return $this->descrEn;
        }
    }

    public function getTitle($lang)
    {
        if ($lang == "sk") {
            return $this->title;
        } else {
            return $this->titleEn;
        }
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @return mixed
     */
    public function getPathThumbnail()
    {
        return $this->path_thumbnail;
    }

}