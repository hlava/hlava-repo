<?php

class Login
{
    protected $id, $uid, $user, $hr, $reporter, $editor, $admin, $meno;

 /**
     * @return mixed
     */
    public function getAdmin()
    {
        return $this->admin;
    }

 /**
     * @return mixed
     */
    public function getMeno()
    {
        return $this->meno;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return mixed
     */
    public function getHr()
    {
        return $this->hr;
    }

    /**
     * @return mixed
     */
    public function getReporter()
    {
        return $this->reporter;
    }

    /**
     * @return mixed
     */
    public function getEditor()
    {
        return $this->editor;
    }

}