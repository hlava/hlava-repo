<?php

/**
 * Created by PhpStorm.
 * User: Tavarez
 * Date: 25.2.2017
 * Time: 20:50
 */

class TypNepritomnosti
{
    protected $id, $typ, $skratka, $farba;
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTyp()
    {
        return $this->typ;
    }

    /**
     * @return mixed
     */
    public function getSkratka()
    {
        return $this->skratka;
    }

    public function getFarba()
    {
        return $this->farba;
    }
}