<?php
/**
 * Created by PhpStorm.
 * User: Tavarez
 * Date: 25.2.2017
 * Time: 20:44
 */

class Nepritomnost
{
    protected $id, $zamestnanec, $typ_nepritomnosti, $datum;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getZamestnanec()
    {
        return $this->zamestnanec;
    }

    /**
     * @return mixed
     */
    public function getTypNepritomnosti()
    {
        return $this->typ_nepritomnosti;
    }

    /**
     * @return mixed
     */
    public function getDatum()
    {
        return $this->datum;
    }

}