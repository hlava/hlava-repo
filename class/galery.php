<?php

/**
 * Created by PhpStorm.
 * User: Tavarez
 * Date: 15.5.2017
 * Time: 13:46
 */
class galery
{
    protected $id, $title, $descr, $titleEn, $descrEn;

    public function getDescr($lang)
    {
        if ($lang == "sk") {
            return $this->descr;
        } else {
            return $this->descrEn;
        }
    }

    public function getTitle($lang)
    {
        if ($lang == "sk") {
            return $this->title;
        } else {
            return $this->titleEn;
        }
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


}