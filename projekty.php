<?php
include('layout/head.php');
include('config/db_majo.php');
?>
    <div class="table-responsive">
    <table class="table table-bordered" id="myTable2">
        <tr class='bg-primary'><th><?php text(cislo_projekt)?></th><th><?php text(nazov)?></th><th colspan="2"><?php text(doba_trvania)?></th><th><?php text(coordinator)?></th></tr>
        <?php
        //cislo projektu, nazov, doba riesenia, riesitel
    if ($lang == "sk") {
        $projekty = array(1 => "Medzinárodné projekty", 2 => "VEGA projekty", 3 => "KEGA projekty", 4 => "APVV projekty", 5 => "Iné domáce projekty");
    }
    else{
        $projekty = array(1 => "International projects", 2 => "VEGA projects", 3 => "KEGA projects", 4 => "APVV projects", 5 => "Other home projects");
    }
        foreach ($projekty as $key=>$value) {
            $sql = "SELECT * FROM projekty WHERE skupina =".$key." ORDER BY order_by DESC ;";
            $result = $conn->query($sql);
            if (($result->num_rows) > 0) {
                echo "<tr class='bg-info'><th colspan='5' style='text-align: center'>" . $value . "</th>";
                while ($row = $result->fetch_assoc()) {

                    if ($lang == "sk") {
                        $name = $row['titleSK'];
                    }
                    else {
                        $name = $row['titleEN'];
                    }
                    $number = $row['number'];

                    $years_from = $row['years_from'];
                    $years_to = $row['years_to'];
                    $coordinator = $row['coordinator'];

                    $from = $row['from'];
                    $to = $row['to'];
                    $projectType = $row['projectType'];
                    $partners = $row['partners'];
                    $web = $row['web'];
                    $internalCode = $row['internalCode'];
                    $id = $row['id'];


                    echo "<tr style='cursor:pointer;' data-toggle='modal' class='inform2' data-target='#myModal2' data-id='$id'><td>" . $number . "</td>";
                    echo "<td> $name  </td>";
                    echo "<td>$years_from</td> <td>$years_to</td>";
                    echo "<td> $coordinator </td>";
                    echo "</tr>";
                }
            }
        }
        ?>
    </table>
    </div>


    <div id="myModal2" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Detail</h4>
                </div>
                <div class="modal-body">
                    <div id="textovepole2"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><?php text(close)?></button>
                </div>
            </div>

        </div>
    </div>

<?php
include ('layout/foot.php');