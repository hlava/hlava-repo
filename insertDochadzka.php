<?php
include('layout/include_items.php');
redirectIfNotLogged($user); 
$typToChange = $_POST["editovany_typ"];
$selectedMesiac = $_POST["mesiac"]; 
$selectedRok = $_POST["rok"];
foreach ($_POST as $key => $value) {
    if ($key != "editovany_typ" && $key != "mesiac" && $key != "rok") {

        $vyparsovanePole = str_getcsv($key);
        if ($typToChange == "deleting") {
            if ($value == 1) {    // ak value je 1 znamena to ze bol oznaceny prvok
                var_dump($key);
                if ($vyparsovanePole[2] != 0) {    // obsahuje id typu momentálnej neprítomnosti
                    deleteRow($db, $vyparsovanePole[0], $vyparsovanePole[1], $vyparsovanePole[2]);
                }
            }
        } else {
            if ($value == 1) {    // ak value je 1 znamena to ze bol oznaceny prvok
                if ($vyparsovanePole[2] != 0) {    // obsahuje id typu momentálnej neprítomnosti
                    updateExistingRow($db, $vyparsovanePole[0], $vyparsovanePole[1], $vyparsovanePole[2], $typToChange);
                } elseif ($vyparsovanePole[2] == 0) {  // nebola žiadna nepritomnost
                    createNewRow($db, $vyparsovanePole[0], $vyparsovanePole[1], $typToChange);
                }
            }
        }
    } 
}
 
function updateExistingRow($db, $zamestnanec, $datum, $staryTyp, $novyTyp)
{
    $createStatement = $db->prepare("UPDATE nepritomnost SET zamestnanec = :meno, typ_nepritomnosti = :novyTyp, datum = :datum WHERE zamestnanec = :meno AND typ_nepritomnosti = :staryTyp AND datum = :datum ");
    $createStatement->execute([':meno' => $zamestnanec, ':novyTyp' => $novyTyp, ':staryTyp' => $staryTyp, ':datum' => $datum]);
}

function createNewRow($db, $zamestnanec, $datum, $typ)
{
    $createStatement = $db->prepare("INSERT INTO nepritomnost (zamestnanec, typ_nepritomnosti, datum) VALUES (:meno, :typ, :datum)");
    $createStatement->execute([':meno' => $zamestnanec, ':typ' => $typ, ':datum' => $datum]);
}

function deleteRow($db, $zamestnanec, $datum, $typ)
{
    $createStatement = $db->prepare("DELETE FROM nepritomnost WHERE zamestnanec = ? AND typ_nepritomnosti = ? AND datum = ?");
    $createStatement->execute([$zamestnanec, $typ, $datum]);
}

header('Location: indexDochadzka.php?' . "mesiac=" . $selectedMesiac . "&rok=" . $selectedRok . "&action=show");
