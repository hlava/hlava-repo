<?php
require __DIR__ . '/../class/galery.php';
require __DIR__ . '/../class/galerySingle.php'; 
require __DIR__ . '/../class/login.php';
require __DIR__ . '/../class/Nepritomnost.php';
require __DIR__ . '/../class/Zamestnanec.php';
require __DIR__ . '/../class/TypNepritomnosti.php';
// ------------------------ usefull functions --------------------------------------

/**
 * @param $date 2014-07-25
 * @return bool
 */
function isWeekend($date)
{
    return (date('N', strtotime($date)) >= 6);
}
//vardump variable with formating
function dd($variable){

    echo "<pre>";
    print_r($variable);
    echo "</pre>";
}
/**
 * @return PDO
 */
function connect_to_db($dbengine,$dbhost,$dbuser,$dbpassword,$dbname)
{
    try {
        $pdo = new PDO("" . $dbengine . ":host=$dbhost; dbname=$dbname", $dbuser, $dbpassword);
        $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
        $pdo->exec("set names utf8");
        return $pdo;
    } catch (PDOException $e) { 
        $e->getMessage();
    }
}
 
function afterLoginMess( $email, $variables = null, $db)  
{
    $_SESSION["loggedUser"] = $email;
    if(!is_null($variables)){ 
        $_SESSION['variables'] = $variables;
    }
     $userFromDb = getLoginByUid($db, $email);    
    if ($userFromDb == 0){     
      var_dump(insertLogin($db, $email, $_SESSION['variables'][0]['cn'][0]));  
   }     
   header('Location: loginDashboard.php'); 
}
function redirectToHomeWithMessage($message){
    header('Location: index.php?message='.$message);
}
function redirectJavaScritp(){
       echo '<script type="text/javascript">
                  window.location = "index.php"
               </script>';
}
/**
 * redirect with _get message
 */
function redirectBackWithFalse($message){
    header('Location: index.php?message='.$message);
}
/**
 * @return user, if not logged return null
 */
function loggedUser(){
    if (isset($_SESSION["loggedUser"]) && $_SESSION["loggedUser"] != null) {
        return $_SESSION["loggedUser"];
    } else {
        return false;
    }
}
/**
 * check if user is logged if not redirect to home page using javascript
 */
function redirectIfNotLogged($user){
    if(!$user) {
        echo '<script type="text/javascript">
                  window.location = "login.php"
               </script>';
    }
}
/**
 * @return boolean
 */
function isLogged(){
    if (isset($_SESSION["loggedUser"]) && $_SESSION["loggedUser"] != null) {
        return true;
    } else {
        return false;
    }
}

function getUserName(){
    return $_SESSION['variables'][0]["cn"][0];
}

function text($key, $opt = null){
    @include("resources/strings.php");
    if(is_null($opt)) {
        $lang = isset($_SESSION["lang"]) ? $_SESSION["lang"] : "sk";
        echo $texts[$key][$lang];
    }else{
        echo $texts[$key][$opt];
    }
}

function text_without_echo($key, $opt = null){
    @include("resources/strings.php");
    if(is_null($opt)) {
        $lang = isset($_SESSION["lang"]) ? $_SESSION["lang"] : "sk";
        return $texts[$key][$lang];
    }else{
        return $texts[$key][$opt];
    }
}


// ------------------------ galerry selects --------------------------------------
function getGalleries($db)
{
    $request = $db->prepare("SELECT id, title, descr, titleEn, descrEn FROM galeria");
    $request->setFetchMode(PDO::FETCH_CLASS, "galery");
    return $request->execute() ? $request->fetchAll() : false;
}
function getGalleryById($db,$id)
{
    $request = $db->prepare("SELECT id, title, descr, titleEn, descrEn FROM galeria WHERE id = ?");
    $request->setFetchMode(PDO::FETCH_CLASS, "galery");
    return $request->execute([$id]) ? $request->fetch() : false;
}
function getPhotosOfGallery($db, $galleryId)
{
    $request = $db->prepare("SELECT id, title, descr, fk_galery, titleEn, descrEn, path_thumbnail, path, size FROM galeriaSingle WHERE fk_galery = ?");
    $request->setFetchMode(PDO::FETCH_CLASS, "galerySingle");
    return $request->execute([$galleryId]) ? $request->fetchAll() : false;
}
// ------------------------ login selects --------------------------------------
function getLogins($db)
{
    $request = $db->prepare("SELECT id, uid, user, hr, reporter, editor, admin, meno FROM login");
    $request->setFetchMode(PDO::FETCH_CLASS, "Login"); 
    return $request->execute() ? $request->fetchAll() : false;
}
function getLoginByUid($db, $uid)
{
    $request = $db->prepare("SELECT id, uid, user, hr, reporter, editor, admin, meno FROM login WHERE uid = ?");
    $request->setFetchMode(PDO::FETCH_CLASS, "Login"); 
    return $request->execute([$uid]) ? $request->fetch() : false;  
}
function insertLogin($db, $uid, $meno)   
{
    $stmt = $db->prepare("INSERT INTO login (uid, meno) VALUES (? , ?)");  
    $stmt->execute([$uid, $meno]);
}  

function updateLogin($db, $user, $hr, $reporter, $editor, $admin, $uid)
{
   $createStatement = $db->prepare("UPDATE login SET user = ? , hr = ? , reporter = ? , editor = ? , admin = ? WHERE uid = ?");
   return $createStatement->execute([$user, $hr, $reporter, $editor, $admin, $uid]); 
}

function getLoggedUserFromDb($db){
    if($_SESSION['loggedUser'] != ""){ 
         return getLoginByUid($db, $_SESSION['loggedUser']); 
    }else return null; 
}

// ------------------------ dochadzka selects --------------------------------------


function fetchZamestnanci($db) 
{
    $request = $db->prepare("SELECT *, id, name as meno, surname as priezvisko FROM zamestnanci ORDER BY surname");
    $request->setFetchMode(PDO::FETCH_CLASS, "Zamestnanec");
    return $request->execute() ? $request->fetchAll() : false;
}

function fetchZamestnanec($db, $id)
{
    $request = $db->prepare(" SELECT *, id, name as meno, surname as priezvisko FROM zamestnanci where id = :id");
    $request->setFetchMode(PDO::FETCH_CLASS, "Zamestnanec");
    return $request->execute(array(':id' => $id)) ? $request->fetch() : false;
}
function fetchZamestnanecByUid($db, $uid)
{
    $request = $db->prepare(" SELECT *, id, name as meno, surname as priezvisko FROM zamestnanci where ldapLogin = :uid"); 
    $request->setFetchMode(PDO::FETCH_CLASS, "Zamestnanec"); 
    return $request->execute(array(':uid' => $uid)) ? $request->fetch() : false; 
}
function fetchTypNepritomnosti($db)  
{
    $request = $db->prepare(" SELECT id, typ, skratka, farba FROM typ_nepritomnosti");
    $request->setFetchMode(PDO::FETCH_CLASS, "TypNepritomnosti"); 
    return $request->execute() ? $request->fetchAll() : false;
}

function fetchNepritomnost($db, $date, $id_zamestnanec)
{
    $sqlString = "SELECT id, zamestnanec, typ_nepritomnosti, datum FROM nepritomnost WHERE datum = ? AND zamestnanec = ?";
    $request = $db->prepare($sqlString);
    $request->setFetchMode(PDO::FETCH_CLASS, "Nepritomnost");
    return $request->execute([$date, $id_zamestnanec]) ? $request->fetch() : false;
}
function updateUserProfil($db, $name, $surname, $title1, $title2, $ldapLogin, $photo, $room, $phone, $department, $staffRole, $function, $url, $id) 
{
   $createStatement = $db->prepare("UPDATE zamestnanci SET name = ?, surname = ?, title1 = ?, title2 = ?, ldapLogin = ?, photo = ?, room = ?, phone = ?, department = ?, staffRole = ?, function = ?, url = ? WHERE id = ?");
   return $createStatement->execute([$name, $surname, $title1, $title2, $ldapLogin, $photo, $room, $phone, $department, $staffRole, $function, $url, $id]); 
} 
function createUserProfil($db, $name, $surname, $title1, $title2, $ldapLogin, $photo, $room, $phone, $department, $staffRole, $function, $url)   
{
    $stmt = $db->prepare("INSERT INTO zamestnanci (name, surname, title1, title2, ldapLogin, photo, room, phone, department, staffRole, function, url) VALUES (? ,?, ?,? , ?,? , ?,? , ?,? , ?,?)");   
    $stmt->execute([$name, $surname, $title1, $title2, $ldapLogin, $photo, $room, $phone, $department, $staffRole, $function, $url]);   
}
function volnetemy ($url,$typ){
    $ch = curl_init($url);
    $data = array(
        'filtr_typtemata2' => $typ,
        'pracoviste' => '642',
        'lang' => 'sk',
        'omezit_temata2' => 'Obmedziť'
    );

    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    $result = curl_exec($ch);
    curl_close($ch);

    echo '<table><tr> <th>Typ práce</th> <th>Názov témy</th> <th>Vedúci práce</th> <th>Garantujúce pracovisko</th> <th>Študijný program</th> <th>Riešitelia</th> <th>Anotácia</th></tr>';

    $doc = new DOMDocument();
    libxml_use_internal_errors(true);
    $doc->loadHTML($result);
    $xPath = new DOMXPath($doc);
    $i = 1;
    while (1) {
        $tableRows = $xPath->query('//html/body/div/div/div/form/table[last()]/tbody/tr[' . $i . ']');


        if ($tableRows[0]->childNodes[1]->textContent == "") {
            break;
        }
        if ($tableRows[0]->childNodes[9]->textContent == "--") {

            $annotationURL = 'http://is.stuba.sk' . $tableRows[0]->childNodes[7]->firstChild->firstChild->getAttribute('href');
            $ch1 = curl_init($annotationURL);
            curl_setopt($ch1, CURLOPT_RETURNTRANSFER, TRUE);
            $result1 = curl_exec($ch1);
            curl_close($ch1);

            $doc1 = new DOMDocument();
            libxml_use_internal_errors(true);
            $doc1->loadHTML($result1);
            $xPath1 = new DOMXPath($doc1);
            $annotation = $xPath1->query('//html/body/div/div/div/table[1]/tbody/tr[last()]/td[last()]')[0]->textContent;


            echo '<tr> <td>' . $tableRows[0]->childNodes[1]->textContent . '</td> <td>' . $tableRows[0]->childNodes[2]->textContent . '</td> <td>' . $tableRows[0]->childNodes[3]->textContent . '</td> <td>' . $tableRows[0]->childNodes[4]->textContent . '</td> <td>' . $tableRows[0]->childNodes[5]->textContent . '</td> <td>' . $tableRows[0]->childNodes[9]->textContent . '</td> <td>' . '<button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal' . $i . '">Ukáž</button>' . '</td> </tr>';
            echo '<!-- Modal -->
                  <div class="modal fade" id="myModal' . $i . '" role="dialog">
                    <div class="modal-dialog">

                      <!-- Modal content-->
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Anotácia</h4>
                        </div>
                        <div class="modal-body">
                          <p>' . $annotation . '</p>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Zavrieť</button>
                        </div>
                      </div>

                    </div>
                  </div>';
        }
        $i++;
    }
    echo '</table>';
}