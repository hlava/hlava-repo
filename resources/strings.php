<?php
// ***************** footer links ********************************
$texts = [
    "linkIs" => [
        "sk" => "AIS STU",
        "en" => "AIS STU",
        "link" => "http://is.stuba.sk"
    ],
    "linkRozvrh" => [
        "sk" => "Rozvrh hodín FEI",
        "en" => "Timetable FEI",
        "link" => "http://aladin.elf.stuba.sk/rozvrh/"
    ],
    "linkMoodle" => [
        "sk" => "Moodle FEI",
        "en" => "Moodle FEI",
        "link" => "http://elearn.elf.stuba.sk/moodle/"
    ],
    "linkSski" => [
        "sk" => "SSKI",
        "en" => "SSKI",
        "link" => "http://www.sski.sk/webstranka/"
    ],
    "linkJedalen" => [ 
        "sk" => "Jedáleň STU",
        "en" => "Cafeteria STU",
        "link" => "https://www.jedalen.stuba.sk/WebKredit/"
    ],
    "linkWebmail" => [
        "sk" => "Webmail STU",
        "en" => "Webmail STU",
        "link" => "https://webmail.stuba.sk/"
    ],
    "linkEvidencia" => [
        "sk" => "Evidencia publikácií STU",
        "en" => "University publications",
        "link" => "https://kis.cvt.stuba.sk/i3/epcareports/epcarep.csp?ictx=stu&language=1"
    ],
    "linkCasopis" => [ 
        "sk" => "Časopis OKO",
        "en" => "OKO magazine",
        "link" => "http://okocasopis.sk/"
    ],
// ***************** login strings ********************************
    "welcome" => [
        "sk" => "Vitajte ",
        "en" => "Welcome "
    ],
    "profil" => [
        "sk" => "Profil",
        "en" => "Profile"
    ],
    "logout" => [
        "sk" => "Odhlásiť",
        "en" => "Logout"
    ],
    "login" => [
        "sk" => "Prihlásiť", 
        "en" => "Login"
    ],
    "prihlasovacieMeno" => [
        "sk" => "Prihlasovacie meno",
        "en" => "Username"
    ],
    "pirhlasovacieHeslo" => [
        "sk" => "Heslo",
        "en" => "Password"
    ],
      "role" => [
        "sk" => "Role",
        "en" => "Roles" 
    ],
    // ***************** contacts ********************************
    "kontaktAdresa" => [
        "sk" => "Adresa",
        "en" => "Address"
    ],
    "kontaktSlovakia" => [
        "sk" => "Slovenská republika",
        "en" => "Slovakia"
    ],
    "kontaktTitle" => [
        "sk" => "Kontaktné údaje",
        "en" => "Contacts"
    ],
    "kontaktInstitute" => [
        "sk" => "Ústav automobilovej mechatroniky",
        "en" => "Institute of Automotive Mechatronics"
    ],
    "kontaktSekretariat" => [
        "sk" => "Kontakt na sekretariát",
        "en" => "Secretary office"
    ],
    "kontaktTelephone" => [
        "sk" => "Telefón do zamestnania",
        "en" => "Tel. number"
    ],
    "kontaktPracovisko" => [
        "sk" => "Pracovisko",
        "en" => "Workplace"
    ],
    "kontaktMiestnost" => [
        "sk" => "Miestnosť",
        "en" => "Room"
    ],

    //***************media********************
    "mediaZoznamClankov"=> [
        "sk" => "Zoznam článkov",
        "en" => "List of articles"
    ],
    "mediaMedium"=> [
        "sk" => "Médium",
        "en" => "Medium"
    ],
    "mediaDatum"=> [
        "sk" => "Publikované",
        "en" => "Published"
    ],
    "zam_meno" => [
        "sk" => "Meno",
        "en" => "Name"
    ],
    "zam_miestnost" => [
        "sk" => "Miestnosť",
        "en" => "Room"
    ],
    "zam_klapka" => [
        "sk" => "Klapka",
        "en" => "Extension"
    ],
    "zam_oddelenie" => [
        "sk" => "Oddelenie",
        "en" => "Department"
    ],
    "zam_zaradenie" => [
        "sk" => "Zaradenie",
        "en" => "Official position"
    ],
    "zam_telefon" => [
        "sk" => "Telefón",
        "en" => "Tel. number"
    ],
    "zam_funkcia" => [
        "sk" => "Funkcia",
        "en" => "Function"
    ],
    "zam_filtruj_oddelenie" => [
        "sk" => "Filtruj podľa oddelenia...",
        "en" => "Filter by department..."
    ],
    "zam_filtruj_zaradenie" => [
        "sk" => "Filtruj podľa zaradenia...",
        "en" => "Filter by official position..."
    ],
    "publikacie" => [
        "sk" => "Vybrané publikácie za posledných 5 rokov",
        "en" => "Selected publications from the last 5 years"
    ],
    "publikacia" => [
        "sk" => "Publikácie",
        "en" => "Publications"
    ],
    "rok" => [
        "sk" => "Rok",
        "en" => "Year"
    ],
    "druh_publikacie" => [
        "sk" => "Druh publikácie",
        "en" => "Type of publication"
    ],
    "typ_projekt" => [
        "sk" => "Typ projektu",
        "en" => "Project type"
    ],
    "typ_projekt" => [
        "sk" => "Typ projektu",
        "en" => "Project type"
    ],
    "cislo_projekt" => [
        "sk" => "Číslo projektu",
        "en" => "Project number"
    ],
    "doba_trvania" => [
        "sk" => "Doba trvania",
        "en" => "Duration"
    ],
    "coordinator" => [
        "sk" => "Vedúci projektu",
        "en" => "Coordinator"
    ],
    "partners" => [
        "sk" => "Partneri",
        "en" => "Partners"
    ],
    "web" => [
        "sk" => "Web stránka",
        "en" => "Web page"
    ],
    "internal_code" => [
        "sk" => "Interný kód",
        "en" => "Internal code"
    ],
    "anotacia" => [
        "sk" => "Anotácia",
        "en" => "Annotation"
    ],
    "nazov" => [
        "sk" => "Názov",
        "en" => "Name"
    ],
    "close" => [
        "sk" => "Zavrieť",
        "en" => "Close"
    ],
 // ***************** galeria ********************************
    "galeria" => [
        "sk" => "Galérie",
        "en" => "Galleries"
    ],
    "editRoles" => [
        "sk" => "Editácia rolí",
        "en" => "Edit of roles"
    ],
    "uloz" => [
        "sk" => "Ulož",
        "en" => "Save" 
    ],
 // ***************** dochadzka ********************************
    "mesiac" => [
        "sk" => "Mesiac",
        "en" => "Month" 
    ],
        "rok" => [
        "sk" => "Rok",
        "en" => "Year" 
    ],
        "ukaz" => [
        "sk" => "Ukáž",
        "en" => "Show" 
    ],
        "edit" => [
        "sk" => "Edituj",
        "en" => "Edit mode"
    ], 
        "legenda" => [    
        "sk" => "Legenda",
        "en" => "Legend" 
    ],
        "textDochadzka" => [   
        "sk" => "Typ neprítomnosti zmeň na:",
        "en" => "Change type to:"
    ],
       "osobnyKalend" => [   
        "sk" => "Osobný kalendár",
        "en" => "Personal calendar"   
    ],
        "editujZamestn" => [   
        "sk" => "Edituj zamestnanca:",
        "en" => "Edit employee:"
    ],
        "show" => [     
        "sk" => "Prehľad",
        "en" => "Show mode"
    ],
       "dochadzka" => [     
        "sk" => "Dochádzka", 
        "en" => "Attendance"   
    ],  
     "createUser" => [ 
        "sk" => "Vytvor zamestnanca",
        "en" => "Create new employee"
    ],
      "spravaZamestnancov" => [     
        "sk" => "Správa zamestnancov", 
        "en" => "Employee editation"
    ],  
  "pridajZames" => [      
        "sk" => "Pridaj zamestnanca",  
        "en" => "Add employee"
    ],  
    "vlozVideo" => [      
        "sk" => "Pridaj video",  
        "en" => "Add video"   
    ],  

    // ***************** index ********************************
       "upozornenie" => [     
        "sk" => "upozornenie",
        "en" => "Warning"  
    ], 

       "upozornenieMessage" => [     
        "sk" => "Tvoje prihlasovacie meno nemá oprávnenie",
        "en" => "Access denied!"
    ],

];

