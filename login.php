<?php
include('layout/head.php');
if (!is_null($_SESSION["loggedUser"])) {
    header('Location: dashBoard.php');
} else {
    ?>
    <div class="container">
        <div class="modal-body">
            <div class="well">

                <div class="tab-pane active in" id="login">
                    <form action='loginController.php' method="POST">
                        <div id="legend">
                            <legend class=""><?php text("login");?></legend>
                        </div>
                        <?php if (isset($_GET["message"])) {
                            ?>
                            <p style="color: red"><?php echo $_GET["message"]; ?></p>
                            <?php
                        } ?>
                        <br>
                        <div class="form-group">
                            <label for="exampleInputEmail1"><?php text("prihlasovacieMeno");?></label>
                            <input type="text" class="form-control" id="loginEmail"
                                   placeholder="prihlasovacie udaje" requred name="loginEmail" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1"><?php text("pirhlasovacieHeslo");?></label>
                            <input type="password" class="form-control" id="loginPass"
                                   placeholder="******" requred name="loginPass" required>
                        </div>
                        <div class="control-group">
                            <div class="controls">
                                <button class="btn btn-success">Login</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <?php include('layout/foot.php');

} ?>
