<?php
include('layout/head.php');
?>
<?php

if (isset($_POST['save'])) {
    $email = $_POST['emailAdd'];
    $checkEmail = $db->prepare("SELECT * FROM newsletter WHERE email='$email'");
    $checkEmail->execute();
    $emails = $checkEmail->fetchAll();

    if ($emails == NULL) {
        $insertEmail = $db->prepare("INSERT INTO newsletter (email) VALUES (:email)");
        $insertEmail->execute([':email' => $email]);
    }
}

if (isset($_POST['delete'])) {
    $email = $_POST['emailDel'];
    $checkEmail = $db->prepare("DELETE FROM newsletter WHERE email='$email'");
    $checkEmail->execute();
}


if (isset($_POST['submit'])) {

    $commentSK = $_POST['commentSK'];
    $commentEN = $_POST['commentEN'];
    $kategoria = $_POST['kategoria'];
    $expiracia = $_POST['expiracia'];
    $nazov = $_POST['nazov'];
    $title = $_POST['title'];

    $insertAktualitu = $db->prepare("INSERT INTO aktuality (slov, eng, typ, datum, nazov, title) VALUES (:commentSK, :commentEN, :kategoria, :expiracia, :nazov, :title)");
    $insertAktualitu->execute([':commentSK' => $commentSK, ':commentEN' => $commentEN, ':kategoria' => $kategoria, ':expiracia' => $expiracia, ':nazov' => $nazov, ':title' => $title]);
    $msg = wordwrap($commentSK, 70);
    $sendEmail = $db->prepare("SELECT * FROM newsletter");
    $sendEmail->execute();
    $result = $sendEmail->fetchAll();
    $count = $sendEmail->rowCount();

    for ($i = 0; $i < $count; $i++) {
        mail($result[$i]->email, $nazov, $msg);
    }

}

echo "<h1>Aktuality</h1>";

?>
    <div class="container">
        <?php

        if (isset($_POST['filter'])) {
            if (isset($_POST['1'])) {
                $arguments[] = "typ LIKE '1'";
            }
            if (isset($_POST['2'])) {
                $arguments[] = "typ LIKE '2'";
            }
            if (isset($_POST['3'])) {
                $arguments[] = "typ LIKE '3'";
            }
            if (!empty($arguments)) {
                $str = implode(' or ', $arguments);

                $zobrazAktuality = $db->prepare("SELECT * FROM aktuality WHERE " . $str);
                $zobrazAktuality->execute();
                $result = $zobrazAktuality->fetchAll();

            } else {

                $zobrazAktuality = $db->prepare("SELECT * FROM aktuality");
                $zobrazAktuality->execute();

                $result = $zobrazAktuality->fetchAll();
            }

            $count = $zobrazAktuality->rowCount();
            $today = date("Y-m-d");
            for ($i = 0; $i < $count; $i++) {
                if (isset($_POST['4'])) {
                    if ($lang == "sk") {
                        echo "<div class=\"well\">";
                        echo "<div class=\"modal-content\" style=\"padding: 5px;\">";
                        echo "  <div class=\"container-fluid\">";
                        echo "<h4>" . $result[$i]->nazov . "</h4>";
                        if ($result[$i]->typ == '1') {
                            echo "<em>Propagácia</em><br>";
                        } else if ($result[$i]->typ == '2') {
                            echo "<em>Oznamy</em><br>";
                        } else {
                            echo "<em>Zo života ústavy</em><br>";
                        }
                        echo $result[$i]->slov;
                        echo "</div>";
                        echo "</div>";
                        echo "</div>";
                    }
                    if ($lang == "en" && $result[$i]->eng != NULL) {
                        echo "<div class=\"well\">";
                        echo "<div class=\"modal-content\" style=\"padding: 5px;\">";
                        echo "  <div class=\"container-fluid\">";
                        echo "<h4>" . $result[$i]->title . "</h4>";
                        echo $result[$i]->eng;
                        echo "</div>";
                        echo "</div>";
                        echo "</div>";
                    }

                } else if ($today < $result[$i]->datum) {
                    if ($lang == "sk") {
                        echo "<div class=\"well\">";
                        echo "<div class=\"modal-content\" style=\"padding: 5px;\">";
                        echo "  <div class=\"container-fluid\">";
                        echo "<h4>" . $result[$i]->nazov . "</h4>";
                        if ($result[$i]->typ == '1') {
                            echo "<em>Propagácia</em><br>";
                        } else if ($result[$i]->typ == '2') {
                            echo "<em>Oznamy</em><br>";
                        } else {
                            echo "<em>Zo života ústavy</em><br>";
                        }
                        echo $result[$i]->slov;
                        echo "</div>";
                        echo "</div>";
                        echo "</div>";
                    }
                    if ($lang == "en" && $result[$i]->eng != NULL) {
                        echo "<div class=\"well\">";
                        echo "<div class=\"modal-content\" style=\"padding: 5px;\">";
                        echo "  <div class=\"container-fluid\">";
                        echo "<h4>" . $result[$i]->title . "</h4>";
                        echo $result[$i]->eng;
                        echo "</div>";
                        echo "</div>";
                        echo "</div>";
                    }
                }
            }
        } else {
            $zobrazAktuality = $db->prepare("SELECT * FROM aktuality");
            $zobrazAktuality->execute();

            $result = $zobrazAktuality->fetchAll();
            $count = $zobrazAktuality->rowCount();
            $today = date("Y-m-d");
            for ($i = 0; $i < $count; $i++) {
                if ($today < $result[$i]->datum) {

                    if ($lang == "sk") {
                        echo "<div class=\"well\">";
                        echo "<div class=\"modal-content\" style=\"padding: 5px;\">";
                        echo "  <div class=\"container-fluid\">";
                        echo "<h4>" . $result[$i]->nazov . "</h4>";
                        if ($result[$i]->typ == '1') {
                            echo "<em>Propagácia</em><br>";
                        } else if ($result[$i]->typ == '2') {
                            echo "<em>Oznamy</em><br>";
                        } else {
                            echo "<em>Zo života ústavy</em><br>";
                        }
                        echo $result[$i]->slov;
                        echo "</div>";
                        echo "</div>";
                        echo "</div>";
                    }
                    if ($lang == "en" && $result[$i]->eng != NULL) {
                        echo "<div class=\"well\">";
                        echo "<div class=\"modal-content\" style=\"padding: 5px;\">";
                        echo "  <div class=\"container-fluid\">";
                        echo "<h4>" . $result[$i]->title . "</h4>";
                        echo $result[$i]->eng;
                        echo "</div>";
                        echo "</div>";
                        echo "</div>";
                    }
                }
            }
        } ?>
    </div>

    <div class="container">
        <h3>Aktuality na zobrazenie:</h3>
        <form action="export.php" method="post">
            <div class="form-group">
                <?php
                echo "<div class=\"checkbox\">
  <label><input type=\"checkbox\" value=\"1\"  name=\"1\">Propagácia</label>
</div>
<div class=\"checkbox\">
  <label><input type=\"checkbox\" value=\"2\"  name=\"2\">Oznamy</label>
</div>
<div class=\"checkbox\">
  <label><input type=\"checkbox\" value=\"3\"  name=\"3\">Zo života ústavu</label>
</div>
<div class=\"checkbox\">
  <label><input type=\"checkbox\" value=\"4\" name=\"4\">Aj neaktívne</label>
</div><button type=\"submit\" name = \"filter\" class=\"btn btn-default\">Zobraziť</button>";
                ?>
            </div>
        </form>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-xs-6">
                <h4>Prihlásenie na newsletter:</h4>
                <form action="export.php" method="post">
                    <div class="form-group">
                        <label for="email">E-mail:</label>
                        <textarea class="form-control" rows="1" id="emailAdd" name="emailAdd"
                                  placeholder="meno@mail.com"></textarea>
                    </div>
                    <button type="submit" name="save" class="btn btn-success">Odoberať</button>
                </form>
            </div>

            <div class="col-xs-6">
                <h4>Zrušenie odberu:</h4>
                <form action="export.php" method="post">
                    <div class="form-group">
                        <label for="email">E-mail:</label>
                        <textarea class="form-control" rows="1" id="emailDel" name="emailDel"
                                  placeholder="meno@mail.com"></textarea>
                    </div>
                    <button type="submit" name="delete" class="btn btn-success">Zrušiť</button>
                </form>
            </div>
        </div>
    </div>

<?php
if (isLogged()) {
    if ($userFromDb->getReporter() == 1 || $userFromDb->getAdmin() == 1) {

        ?>
        <!-- Trigger the modal with a button -->
        <div class="well">
            <div class="container-fluid">
                <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Pridať
                    novú/Add
                    new
                    post
                </button>
            </div>
        </div>
        <!-- Modal -->
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Pridanie/Add</h4>
                    </div>
                    <div class="modal-body">
                        <form action="export.php" method="post">
                            <div class="form-group">
                                <label for="nazov">Názov:</label>
                                <textarea class="form-control" rows="1" id="nazov" name="nazov"
                                          placeholder="Slovenský názov"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="comment">Nová aktualita:</label>
                                <textarea class="form-control" rows="5" id="commentSK" name="commentSK"
                                          placeholder="Slovenský text" maxlength="200"></textarea>
                                <label for="title">Title:</label>
                                <textarea class="form-control" rows="1" id="title" name="title"
                                          placeholder="In English"></textarea>
                                <label for="comment">New post:</label>
                                <textarea class="form-control" rows="5" id="commentEN" name="commentEN"
                                          placeholder="In English" maxlength="200"></textarea>
                            </div>
                            <label for="kategoria">Kategória/Category:</label>
                            <select class="form-control" id="kategoria" name="kategoria">
                                <option value="1">Propagácia/Publicity</option>
                                <option value="2">Oznamy/Notices</option>
                                <option value="3">Zo života ústavu/Life in the department</option>
                            </select>
                            <div class="form-group">
                                <label for="expiracia">Aktuálne do/Expiration:</label>
                                <input type="date" name="expiracia">
                            </div>
                            <button type="submit" name="submit" class="btn btn-success">Uložiť/Save</button>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Späť/Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
} ?>

<?php include('layout/foot.php');
?>