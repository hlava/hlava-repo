var markers = new Array();
var p = {
    "pamiatky": [
        {"nazov": "FEI STU", "sirka": "48.151842", "dlzka": "17.073365"},
    ]
};
function myMap() {
    var centerSVK = {lat: 48.151842, lng: 17.073365};
    var map = new google.maps.Map(document.getElementById("googleMap1"), {
        center: centerSVK,
        zoom: 15,
    });

    var velkost = Object.keys(p.pamiatky).length;

    for (var i = 0; i < velkost; i++) {
        var miesto = {lat: parseFloat(p.pamiatky[i].sirka), lng: parseFloat(p.pamiatky[i].dlzka)};
        var contentString = "<strong>Fakulta elektrotechniky a informatiky - STU</strong><br>Ilkovičova 3<br>812 19 Bratislava<br>Slovenská republika<br>";
        var marker = new google.maps.Marker({
            position: miesto,
            map: map,
            title: p.pamiatky[i].nazov
        });

        var infowindow = new google.maps.InfoWindow({
            content: contentString
        });

        marker.infowindow = infowindow;

        google.maps.event.addListener(marker, 'click', function () {
            this.infowindow.open(map, this);
        });
    }
}