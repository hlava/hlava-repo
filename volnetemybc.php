<?php
include('layout/head.php');
?>
<style>
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }

    tr:nth-child(even) {
        background-color: #dddddd;
    }
</style>
    <h2>
        Voľné témy
    </h2><br>

<?php
volnetemy('http://is.stuba.sk/pracoviste/prehled_temat.pl',1);
?>


<?php include('layout/foot.php'); ?>