<?php
include('layout/head.php');
?>
    <h2>
        Oddelenie aplikovanej mechaniky a mechatroniky (OAMM)
    </h2><br>
    <div class="panel panel-info">
        <div class="panel-heading"><p><label style="font-size: medium;font-weight: normal;">Vedúci: </label> &nbsp; <label> prof. Ing. Justín Murín, DrSc.</label></p>
            <p><label style="font-size: medium;font-weight: normal;">Zástupca: </label> &nbsp; <label> doc. Ing. Vladimír Kutiš, PhD.</label></p></div>
        <div class="panel-body"> <p>Oddelenie v rámci pedagogiky zabezpečuje v bakalárskom stupni ŠP výučbu predmetov s hlavným dôrazom na mechaniku a mechatronické prvky. V inžinierskom stupni ŠP zabezpečuje výučbu predmetov s dôrazom na simuláciu a modelovanie mechanických a mechatronických systémov tak z pohľadu mechaniky a dynamiky, ako aj z pohľadu multifyzikálneho previazania jednotlivých fyzikálnych domén.</p>
            <p>Členovia oddelenia sa venujú formulácii nových matematických postupov a metód, ktoré sa používajú v multifyzikálnych analýzach napr. na opis funkcionálne gradovaných materiálov (FGM), v dynamických analýzach mechatronických a MEMS systémov, ako aj na opis piezoelektrických prvkov.</p>
            <p>Členovia oddelenia využívajú moderné SW prostriedky, ako sú ANSYS, Catia a MSC.ADAMS na návrh, analýzu a optimalizáciu jednotlivých komponentov, ako aj celých subsystémov mechatronických prvkov.</p>
        </div>
    </div>

<?php include('layout/foot.php'); ?>