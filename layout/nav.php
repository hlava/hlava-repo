<?php
foreach ($nav_items as $nav_item) {
    if (count($nav_item["children"]) == 0) {
        ?>
        <li>
            <a href=" <?php echo $nav_item["url"]; ?> ">
                <?php echo $nav_item["title"][$lang]; ?>
            </a>
        </li>
        <?php
    } else {
        ?>
        <li>
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <?php echo $nav_item["title"][$lang]; ?> <b
                    class="caret"></b></a>
            <ul class="dropdown-menu">
                <?php
                foreach ($nav_item["children"] as $sub_menu) {
                    if (count($sub_menu["children"]) == 0) {

                        ?>
                        <li>
                            <a href="<?php echo $sub_menu["url"]; ?>"><?php echo $sub_menu["title"][$lang]; ?></a>
                        </li>
                        <?php
                    } else {
                        ?>
                        <li class="dropdown-submenu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $sub_menu["title"][$lang]; ?>
                                <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <?php
                                foreach ($sub_menu["children"] as $sub_sub_menu) {
                                    ?>
                                    <li>
                                        <a href="<?php echo $sub_sub_menu["url"]; ?>"><?php echo $sub_sub_menu["title"][$lang]; ?></a>
                                    </li>
                                    <?php
                                }
                                ?>
                            </ul>
                        </li>
                        <?php
                    }
                }
                ?>
            </ul>
        </li>
        <?php
    }
}
?>
<script>
    $(document).ready(function () {
        $('.navbar a.dropdown-toggle').on('click', function (e) {
            var $el = $(this);
            var $parent = $(this).offsetParent(".dropdown-menu");
            $(this).parent("li").toggleClass('open');

            if (!$parent.parent().hasClass('nav')) {
                $el.next().css({"top": $el[0].offsetTop, "left": $parent.outerWidth() - 4});
            }
            $('.nav li.open').not($(this).parents("li")).removeClass("open");

            return false;
        });
    });
</script>

