</div>
</div>
<div class="container" id="footerContainer">
    <footer>
        <div class="container">
            <hr>
            <div class="row">
                <div class="col-md-9">
                    <div class="row">
                        <ul class="list-unstyled col-sm-3">
                            <li><a href="<?php text("linkIs","link");?>" ><?php text("linkIs");?></a> </li>
                            <li><a href="<?php text("linkRozvrh","link");?>" ><?php text("linkRozvrh");?></a> </li>
                        </ul>
                        <ul class="list-unstyled col-sm-3">
                            <li><a href="<?php text("linkMoodle","link");?>" ><?php text("linkMoodle");?></a> </li>
                            <li><a href="<?php text("linkSski","link");?>" ><?php text("linkSski");?></a> </li>
                        </ul>
                        <ul class="list-unstyled col-sm-3">
                            <li><a href="<?php text("linkJedalen","link");?>" ><?php text("linkJedalen");?></a> </li>
                            <li><a href="<?php text("linkCasopis","link");?>" ><?php text("linkCasopis");?></a> </li>
                        </ul>
                        <ul class="list-unstyled col-sm-3">
                            <li><a href="<?php text("linkWebmail","link");?>" ><?php text("linkWebmail");?></a> </li>
                            <li><a href="<?php text("linkEvidencia","link");?>" ><?php text("linkEvidencia");?></a> </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 footer-buttons">
                    <a href="https://www.facebook.com/UAMTFEISTU" title="UAMTFEISTU on Facebook"><span
                            class="fa fa-facebook-square fa-2x">&nbsp;</span></a>
                    <a href="https://www.youtube.com/channel/UCo3WP2kC0AVpQMIiJR79TdA" title="UAMTFEISTU"><span
                            class="fa fa-youtube-square fa-2x">&nbsp;</span></a>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-6">
                    <p> <?php echo $foot_text; ?></p>
                </div>
            </div>
        </div>
    </footer>
</div>
<?php include('links_foot.php'); ?>
</body>
</html>