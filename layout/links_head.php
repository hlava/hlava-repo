<link rel='stylesheet' href='css/bootstrap.min.css' media='all'>
<link rel='stylesheet' href='css/hlavaStyly.css'>
<link rel="stylesheet"  href="css/datatables.min.css">
<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'>
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css">
<script src="js/jquery.min.2.2.3.js"></script>

<!--fotogaleria-->
<link rel="stylesheet"  href="js/PhotoSwipe/dist/photoswipe.css">
<link rel="stylesheet" href="js/PhotoSwipe/dist/default-skin/default-skin.css">

<script src="js/PhotoSwipe/dist/photoswipe-ui-default.min.js"></script>
<script src="js/PhotoSwipe/dist/photoswipe.min.js"></script> 

<!-----insertVideo----->
<script src="js/insertVideo.js" type="text/javascript"></script>