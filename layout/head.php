<!DOCTYPE html>
<html lang="en">
<head>
    <?php include('include_items.php'); ?>
    <meta charset="UTF-8">
    <link rel="icon" href="<?php echo $icon ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $page_title; ?></title>
    <?php include("links_head.php"); ?>
</head>

<body>
<div class="container" id="obsahContainer">
    <nav class="navbar navbar-default ">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span> 
                </button>
                <a class="navbar-brand" href="index.php"><?php echo $nav_title ?></a>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav">
                    <?php include('nav.php'); ?>
                </ul>
                <ul class="nav navbar-nav navbar-right">

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-expanded="false">
                            <?php echo strtoupper($lang); ?></php> <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="changeLang.php?lang=sk"
                                   name="lang" <?php if ($lang == "sk") echo "style='background-color: #e7e7e7;'" ?>>SK</a>
                            </li>
                            <li><a href="changeLang.php?lang=en"
                                   name="lang" <?php if ($lang == "en") echo "style='background-color: #e7e7e7;'" ?>>
                                    EN</a>
                            </li>
                        </ul>
                    </li>
                    <?php
                    if (!isLogged()) {
                        ?>
                        <li>
                            <a href="login.php">
                                <?php text("login");?>
                            </a>
                        </li>
                        <?php
                    }
                    ?>
                    <?php
                    if (isLogged()) {
                        ?>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-expanded="false">
                                <?php echo getUserName($user);?> <span class="caret"></span> 
                            </a> 
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="loginDashboard.php"><i class="fa fa-btn fa-user"></i> <?php text("profil");?></a></li>
                                <li><a href="indexDochadzka.php"><i class="fa fa-calendar" aria-hidden="true"></i>
<?php text("dochadzka");?></a></li>
                                <?php 
                                $userFromDb = getLoggedUserFromDb($db);
                                if($userFromDb->getAdmin() || $userFromDb->getHr()){   
                                    ?>
   <li><a href="showUsers.php"><i class="fa fa-users" aria-hidden="true"></i>
 <?php text("spravaZamestnancov");?></a></li> 
                                <?php
                                }
                                    if($userFromDb->getAdmin()){
                                        ?> 
                                <li><a href="editRoles.php"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> <?php text("editRoles");?></a> 
                                        <?php
                                    }
                                
                                if($userFromDb->getAdmin() || $userFromDb->getReporter()){   
                                    ?>
                                <li><a href="insertVideo.php"><i class="fa fa-caret-square-o-right" aria-hidden="true"></i>
                                    <?php text("vlozVideo");?></a></li> 
                                <?php
                                }
                                ?>
                                <li><a href="logout.php"><i class="fa fa-btn fa-sign-out"></i> <?php text("logout");?></a>
                                </li>
                            </ul>
                        </li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
        </div>
    </nav>

    <div class="container-fluid">