<?php
@include("config/config.php");
//@include("resources/strings.php");
@include("resources/functions.php");
session_start();
$lang = isset($_SESSION["lang"]) ? $_SESSION["lang"] : "sk";
$user = loggedUser();
$db = connect_to_db($dbengine, $dbhost, $dbuser, $dbpassword, $dbname);
// ------------------------ nazov stranky ---------------------------------------------
$page_title = "UAMT";
// ------------------------ text pre footer --------------------------------------------
$foot_text = "© Copyright 2017 " . $page_title;
// ------------------------ nazov pre hlavný navigačný bar -----------------------------
$nav_title = "<strong>$page_title</strong>";
// ------------------------ baklažán ---------------------------------------------------
$icon = "images/icon.png";
// ------------------------ jednotlive prvky navigacneho baru --------------------------
$nav_items =
    [
        [
            "title" => [
                "sk" => "Čo je to mechatronika?",
                "en" => "What are mechatronics?"
            ],
            "url" => "wtfmecha.php",
            "children" => []
        ],
        [
            "title" => [
                "sk" => "O nás",
                "en" => "About us"
            ],
            "url" => "index.php",
            "children" => [
                [
                    "title" => [
                        "sk" => "História",
                        "en" => "History"
                    ],
                    "url" => "history.php",
                    "children" => []
                ],
                [
                    "title" => [
                        "sk" => "Vedenie ústavu",
                        "en" => "Institute leadership"
                    ],
                    "url" => "vedenieustavu.php",
                    "children" => []
                ],
                [
                    "title" => [
                        "sk" => "Oddelenia",
                        "en" => "Departments"
                    ],
                    "url" => "index.php",
                    "children" => [
                        [
                            "title" => [
                                "sk" => "Oddelenie aplikovanej mechaniky a mechatroniky (OAMM)",
                                "en" => "Department of mechanics and mechatronics (OAMM)"
                            ],
                            "url" => "OAMM.php",
                            "children" => []
                        ],
                        [
                            "title" => [
                                "sk" => "Oddelenie informačných, komunikačných a riadiacich systémov (OIKR)",
                                "en" => "Department of information, communication and control systems (OIKR)"
                            ],
                            "url" => "OIKR.php",
                            "children" => []
                        ],
                        [
                            "title" => [
                                "sk" => "Oddelenie elektroniky, mikropočítačov a PLC systémov (OEMP)",
                                "en" => "Department of electronics, microcomputers and PLC (OEMP)"
                            ],
                            "url" => "OEMP.php",
                            "children" => []
                        ],
                        [
                            "title" => [
                                "sk" => "Oddelenie E-mobility, automatizácie a pohonov (OEAP)",
                                "en" => "Department of E-mobility, automation and drives (OEAP)"
                            ],
                            "url" => "OEAP.php",
                            "children" => []
                        ],
                    ]
                ],
            ]
        ],
        [
            "title" => [
                "sk" => "Pracovníci",
                "en" => "Staff"
            ],
            "url" => "zamestnanci.php",
            "children" => []
        ],
        [
            "title" => [
                "sk" => "Štúdium",
                "en" => "Study"
            ],
            "url" => "index.php",
            "children" => [
                [
                    "title" => [
                        "sk" => "Pre uchádzačov o štúdium",
                        "en" => "For applicants about the studies"
                    ],
                    "url" => "index.php",
                    "children" => [
                        [
                            "title" => [
                                "sk" => "Bakalárske štúdium",
                                "en" => "Bachelor study"
                            ],
                            "url" => "bakalar.php",
                            "children" => []
                        ],
                        [
                            "title" => [
                                "sk" => "Inžinierske štúdium",
                                "en" => "Engineering study"
                            ],
                            "url" => "ing.php",
                            "children" => []
                        ],
                    ]
                ],
                [
                    "title" => [
                        "sk" => "Bakalárske štúdium",
                        "en" => "Bachelor study"
                    ],
                    "url" => "index.php",
                    "children" => [
                        [
                            "title" => [
                                "sk" => "Všeobecné informácie",
                                "en" => "General information"
                            ],
                            "url" => "vseobinfo.php",
                            "children" => []
                        ],
                        [
                            "title" => [
                                "sk" => "Pokyny",
                                "en" => "Instructions"
                            ],
                            "url" => "bcpokyny.php",
                            "children" => []
                        ],
                        [
                            "title" => [
                                "sk" => "Voľné témy",
                                "en" => "Free thesises"
                            ],
                            "url" => "volnetemybc.php",
                            "children" => []
                        ],

                    ]
                ],
                [
                    "title" => [
                        "sk" => "Inžinierske štúdium",
                        "en" => "Engineering study"
                    ],
                    "url" => "index.php",
                    "children" => [
                        [
                            "title" => [
                                "sk" => "Všeobecné informácie",
                                "en" => "General information"
                            ],
                            "url" => "vseobinfoing.php",
                            "children" => []
                        ],
                        [
                            "title" => [
                                "sk" => "Pokyny",
                                "en" => "Instructions"
                            ],
                            "url" => "ingpokyny.php",
                            "children" => []
                        ],
                        [
                            "title" => [
                                "sk" => "Voľné témy",
                                "en" => "Free thesises"
                            ],
                            "url" => "volnetemying.php",
                            "children" => []
                        ],
                    ]
                ],
                [
                    "title" => [
                        "sk" => "Doktorandské štúdium",
                        "en" => "Doctoral studies"
                    ],
                    "url" => "doctor.php",
                    "children" => []
                ],
            ]
        ],
        [
            "title" => [
                "sk" => "Výskum",
                "en" => "Research"
            ],
            "url" => "index.php",
            "children" => [
                [
                    "title" => [
                        "sk" => "Projekty",
                        "en" => "Projects"
                    ],
                    "url" => "projekty.php",
                    "children" => []
                ],
                [
                    "title" => [
                        "sk" => "Výskumné oblasti",
                        "en" => "Research topics"
                    ],
                    "url" => "index.php",
                    "children" => [
                        [
                            "title" => [
                                "sk" => "Elektrická motokára",
                                "en" => "Electric cars"
                            ],
                            "url" => "ekart.php",
                            "children" => []
                        ],
                        [
                            "title" => [
                                "sk" => "Autonómne vozidlo 6×6",
                                "en" => "Car 6×6"
                            ],
                            "url" => "autvozidlo.php",
                            "children" => []
                        ],
                        [
                            "title" => [
                                "sk" => "3D LED kocka",
                                "en" => " 3D LED cube"
                            ],
                            "url" => "3Dkocka.php",
                            "children" => []
                        ],
                        [
                            "title" => [
                                "sk" => "Biomechatronika",
                                "en" => "Biomechatronics"
                            ],
                            "url" => "biomecha.php",
                            "children" => []
                        ],
                    ]
                ],
            ]
        ],
        [
            "title" => [
                "sk" => "Aktuality",
                "en" => "News"
            ],
            "url" => "export.php",
            "children" => []
        ],
        [
            "title" => [
                "sk" => "Aktivity",
                "en" => " Activities"
            ],
            "url" => "index.php",
            "children" => [
                [
                    "title" => [
                        "sk" => "Fotogaléria",
                        "en" => "Photos"
                    ],
                    "url" => "gallery.php",
                    "children" => []
                ],
                [
                    "title" => [
                        "sk" => "Videá",
                        "en" => "Video"
                    ],
                    "url" => "videa.php",
                    "children" => []
                ],
                [
                    "title" => [
                        "sk" => "Médiá",
                        "en" => "Media"
                    ],

                    "url" => "media.php",
                    "children" => []
                ],
                [
                    "title" => [
                        "sk" => "Naše tématické web stránky",
                        "en" => "Our thematic website"
                    ],
                    "url" => "index.php",
                    "children" => [
                        [
                            "title" => [
                                "sk" => "Elektromobilita",
                                "en" => "Elektromobility"
                            ],
                            "url" => "http://www.e-mobilita.fei.stuba.sk",
                            "children" => []
                        ],
                    ]
                ],
            ]
        ],
        [
            "title" => [
                "sk" => "Kontakt",
                "en" => " Contact"
            ],
            "url" => "kontakty.php",
            "children" => []
        ],
        [
            "title" => [
                "sk" => "Úlohy",
                "en" => "Tasks"
            ],
            "url" => "rozdelenie.php",
            "children" => []
        ],
    ];

