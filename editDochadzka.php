<?php   
redirectIfNotLogged($user); 
 
$canEditEverything = 0;
 $userFromDb = getLoggedUserFromDb($db);                           
if($userFromDb->getAdmin() || $userFromDb->getHr()){
$zamestnanci = fetchZamestnanci($db); 
$canEditEverything = 1;
}else{
$zamestnanci = [fetchZamestnanecByUid($db,$userFromDb->getUid())]; 
}
$typy_nepritomnosti = fetchTypNepritomnosti($db);
$_POST['mesiac'] = $_GET['mesiac'];
$_POST['rok'] = $_GET['rok'];
function find_typ_nepritomnosti($typy_nepritomnosti, $id_typu)
{
    foreach ($typy_nepritomnosti as $typ_nepri) {
        if ($typ_nepri->getId() == $id_typu) {
            return $typ_nepri;
        }
    } 
} ?>
<script> 
    function validateForm() {
//        var asdf = document.getElementsByClassName("highlighted");
//        for (var i = 0; i < asdf.length; i++) {
//            asdf[i].firstElementChild.setAttribute("value", 1);
//        }
        return true;
 
    }
    $(function () { 
        var isMouseDown = false;
        var rowL = 0;
        $(".table td") 
            .mousedown(function () {  
        rowL = $(this).parent().parent().children().index($(this).parent());
                isMouseDown = true;
                 if ($(this).hasClass('highlighted')){  
                      $(this).removeClass("highlighted"); 
                      $(this).children("input").attr("value", 0); 
                 }else{
                    $(this).toggleClass("highlighted");
                    $(this).children("input").attr("value", 1);
                } 
                return false; // prevent text selection
            })
            .mouseover(function () { 
                if (isMouseDown) { 
                    if($(this).parent().parent().children().index($(this).parent()) == rowL){
                       if ( $(this).hasClass('highlighted')){
                          $(this).removeClass("highlighted");
                          $(this).children("input").attr("value", 0); 
                     }else{
                        $(this).toggleClass("highlighted");
                        $(this).children("input").attr("value", 1);
                    } 
                } 
                }
            }); 

        $(document)
            .mouseup(function () {
                isMouseDown = false;
            });
    });
</script>  
<form method="post" onsubmit="return validateForm()" action="insertDochadzka.php"> 
    <input name="mesiac" type="hidden" value="<?php echo $selected_month; ?>">
    <input name="rok" type="hidden" value="<?php echo $selected_year; ?>">
    <label for="editovany_typ"><?php text('textDochadzka');?></label>
    <div class="form-inline">
        <select class="form-control" id="editovany_typ" name="editovany_typ">
            
            <?php
            if($canEditEverything == 0){
?>
<option value="1">PN - Práce neschopný</option>
<?php
            }else{
                ?>
<option value="deleting">Vymazávanie neprítomnosti</option>
                <?php
            foreach ($typy_nepritomnosti as $typNepri) {
                echo "<option value='" . $typNepri->getId() . "'>" . $typNepri->getSkratka() . " - " . $typNepri->getTyp() . "</option>";
            }
        }
            ?>
        </select>
    </div>

    <div class="table-responsive">
        <table class="table" id="table_id">
            <thead> 
            <tr>
            <th><?php text('zam_meno');?> </th>
                <?php   
                    for ($tmp = 1; $tmp <= $days_in_month; $tmp++) {
                    if (isWeekend($selected_year . "-" . $selected_month . "-" . $tmp)) {
                        echo "<th style='color: red'>$tmp</th>";
                    } else {
                        echo "<th>$tmp</th>";
                    }
                }
                ?>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($zamestnanci as $zamestnanec) {
                echo " <tr>";
                ?> 
                <th style="cursor: pointer" data-toggle="modal"
                    data-target="#myModal_<?php echo $zamestnanec->getId() ?>">
                    <b><?php echo $zamestnanec->getCeleMeno() ?></b></th>
                <?php

                for ($tmp = 1; $tmp <= $days_in_month; $tmp++) {
                    $dochadzka = fetchNepritomnost($db, $selected_year . "-" . $selected_month . "-" . $tmp, $zamestnanec->getId());
                    if ($dochadzka) {
                        $tmpTyp = find_typ_nepritomnosti($typy_nepritomnosti, $dochadzka->getTypNepritomnosti());
                        ?>
                        <td style="color:<?php echo $tmpTyp->getFarba(); ?>;"> <?php echo $tmpTyp->getSkratka(); ?>
                            <input class="biohazard" type="hidden"
                                   name="<?php echo $zamestnanec->getId() . "," . $selected_year . "-" . $selected_month . "-" . $tmp . "," . $tmpTyp->getId() ?>"
                                   value="0">
                        </td>
                        <?php
                    } else {
                        ?>
                        <td>
                            <input class="biohazard" type="hidden"
                                   name="<?php echo $zamestnanec->getId() . "," . $selected_year . "-" . $selected_month . "-" . $tmp . "," . "0" ?>"
                                   value="0">
                        </td>
                        <?php
                    }
                }
                echo "</tr>";
            } ?>
            </tbody>
        </table>
    </div>
    <br>
    <div class="col-md-4 col-md-offset-4">
        <button type="submit" class="btn btn-primary btn-block">
            <i class="fa fa-floppy-o" aria-hidden="true"></i> <?php text('uloz');?>
        </button>
    </div>
    <br>

</form>
<br><br>


<?php
foreach ($zamestnanci as $zamestnanec) {
    ?>
    <!-- Modal -->
    <div class="modal fade" id="myModal_<?php echo $zamestnanec->getId() ?>" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content--> 
            <div class="modal-content">
                <form method="post" onsubmit="return validateForm()" action="insertDochadzka.php">
                    <input name="mesiac" type="hidden" value="<?php echo $selected_month; ?>">
                    <input name="rok" type="hidden" value="<?php echo $selected_year; ?>">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"><?php text('osobnyKalend');?></h4>
                    </div>

                    <div class="modal-body">
                        <p><?php text('editujZamestn');?><b> <?php echo $zamestnanec->getCeleMeno() ?></b></p>
                        <label for="editovany_typ_2"><?php text('textDochadzka');?></label> 
                        <div class="form-inline">
                        <select class="form-control" id="editovany_typ_2" name="editovany_typ">
            
            <?php
            if($canEditEverything == 0){
?>
<option value="1">PN - Práce neschopný</option>
<?php
            }else{
                ?>
<option value="deleting">Vymazávanie neprítomnosti</option>
                <?php 
            foreach ($typy_nepritomnosti as $typNepri) {
                echo "<option value='" . $typNepri->getId() . "'>" . $typNepri->getSkratka() . " - " . $typNepri->getTyp() . "</option>";
            } 
        }
            ?>
        </select>
                        
                        </div>
                        <hr>
                        <div class="table-responsive">
                            <?php
                            $month = $selected_month;
                            $year = $selected_year;
                            // Create array containing abbreviations of days of week.
                            if($lang == "sk"){
                            $daysOfWeek = array('Pon', 'Ut', 'Str', 'Štv', 'Pia', 'Sob', 'Ned');
}else{
                            $daysOfWeek = array('Mon', 'Tue', 'Wed' ,'Thur', 'Fri' ,'Sat' ,'Sun');
}
                            // What is the first day of the month in question?
                            $firstDayOfMonth = mktime(0, 0, 0, $month, 1, $year);
                            // How many days does this month contain?
                            $numberDays = date('t', $firstDayOfMonth);
                            // Retrieve some information about the first day of the
                            // month in question.
                            $dateComponents = getdate($firstDayOfMonth);
                            // What is the name of the month in question?
                            $monthName = $dateComponents['month'];
                            // What is the index value (0-6) of the first day of the
                            // month in question.
                            $dayOfWeek = $dateComponents['wday'] -1;
                            if ($dayOfWeek < 0) {
                                $dayOfWeek = 6;
                            }
                            // Create the table tag opener and day headers
                            $calendar = "<table class='table'>";
                            $calendar .= "<caption>$mesiac[$selected_month]  $year</caption>";
                            $calendar .= "<tr>";

                            // Create the calendar headers

                            foreach ($daysOfWeek as $day) {
                                $calendar .= "<th>$day</th>";
                            }

                            // Create the rest of the calendar

                            // Initiate the day counter, starting with the 1st.

                            $currentDay = 1;

                            $calendar .= "</tr><tr>";

                            // The variable $dayOfWeek is used to
                            // ensure that the calendar
                            // display consists of exactly 7 columns.
                            if ($dayOfWeek > 0) {
                                $calendar .= "<th colspan='$dayOfWeek'>&nbsp;</td>";
                            }

                            $month = str_pad($month, 2, "0", STR_PAD_LEFT);

                            while ($currentDay <= $numberDays) {

                                // Seventh column (Saturday) reached. Start a new row.

                                if ($dayOfWeek == 7) {

                                    $dayOfWeek = 0;
                                    $calendar .= "</tr><tr>";
                                }
                                $currentDayRel = str_pad($currentDay, 2, "0", STR_PAD_LEFT);
                                $date = "$year-$month-$currentDayRel";


                                $dochadzka = fetchNepritomnost($db, $selected_year . "-" . $selected_month . "-" . $currentDay, $zamestnanec->getId());
                                if ($dochadzka) {
                                    $tmpTyp = find_typ_nepritomnosti($typy_nepritomnosti, $dochadzka->getTypNepritomnosti());
                                    $calendar .= "<td rel='$date'>$currentDay <b style='color: " . $tmpTyp->getFarba() . "'>" . $tmpTyp->getSkratka() . "</b>";
                                    $calendar .= "<input value='0' class='biohazard'  type='hidden' name='" . $zamestnanec->getId() . "," . $selected_year . "-" . $selected_month . "-" . "$currentDay" . "," . $tmpTyp->getId() . "' ></input></td>";
                                } else {
                                    $calendar .= "<td class='day' rel='$date'>$currentDay";
                                    $calendar .= "<input value='0' class='biohazard'  type='hidden' name='" . $zamestnanec->getId() . "," . $selected_year . "-" . $selected_month . "-" . "$currentDay" . ",0' ></input></td>";
                                }

                                // Increment counters
                                $currentDay++;
                                $dayOfWeek++;
                            }
                            // Complete the row of the last week in month, if necessary
                            if ($dayOfWeek != 7) {
                                $remainingDays = 7 - $dayOfWeek;
                                $calendar .= "<th colspan='$remainingDays'>&nbsp;</th>";
                            }
                            $calendar .= "</tr>";
                            $calendar .= "</table>";
                            echo $calendar;
                            ?>
                        </div>
                    </div>
                    <div class="modal-footer"> 
                        <div class="col-md-4 col-md-offset-4">
                            <button type="submit" class="btn btn-primary btn-block">
                                <i class="fa fa-floppy-o" aria-hidden="true"></i> <?php text('uloz');?>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <?php
}
?>