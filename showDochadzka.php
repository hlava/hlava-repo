<?php
$zamestnanci = fetchZamestnanci($db);
$typy_nepritomnosti = fetchTypNepritomnosti($db); 

function find_typ_nepritomnosti($typy_nepritomnosti,$id_typu){
    foreach($typy_nepritomnosti as $typ_nepri){
        if($typ_nepri->getId() == $id_typu){
            return $typ_nepri; 
        }
    }
}
?>
<h2> <?php text('show');?> </h2>  
<div class="table-responsive"> 
    <table class="table table-striped table-hover dataTablePdf">    
        <thead>
        <tr>  
            <?php
            echo "<th>meno</th>";
            for ($tmp = 1; $tmp <= $days_in_month; $tmp++) {
                if (isWeekend($selected_year . "-" . $selected_month . "-" . $tmp)) {
                    echo "<th style='color: red'>$tmp</th>";
                } else {
                    echo "<th>$tmp</th>";

                }
            }
            ?>
        </tr>

        </thead>

        <tbody>

        <?php
        $tmp2 = 1;

        foreach ($zamestnanci as $zamestnanec) {
            echo " <tr>";
            echo "<td><b>" . $zamestnanec->getCeleMeno() . "</b></td>";  

            for ($tmp = 1; $tmp <= $days_in_month; $tmp++) {
                $dochadzka = fetchNepritomnost($db, $selected_year . "-" . $selected_month . "-" . $tmp, $zamestnanec->getId());
                if ($dochadzka) {
                    $tmpTyp = find_typ_nepritomnosti($typy_nepritomnosti,$dochadzka->getTypNepritomnosti());
                    echo "<td style='color: ".$tmpTyp->getFarba()."'>".$tmpTyp->getSkratka()."</td>";
                } else {
                    echo "<td></td>";
                }
            }
            echo "</tr>"; 
        } ?>
        </tbody>
    </table>
</div>
<br><br>