<?php
include('layout/head.php');
?>
    <h2>
        3D LED kocka
    </h2><br>

    <div style="width: 50%; float: right"">
    <?php if ($lang == "sk") { ?>
        <p>Zobrazená kocka bola vytvorená v rámci diplomovej práce. Bol k nej vytvorený vzdialený prístup cez Internet, čo umožňuje užívateľovi vkladať do kocky vlastný kód, ktorý ovplyvňuje jej správanie sa.</p>
<?php  }
    if ($lang == "en") {
        ?>  <p>Displayed cube was created within the diploma thesis. It was created for remote access via the Internet. It allows the user to insert custom code blocks and in this way to influence its behavior.</p>
  <?php  }?>
    </div>

    <div style="width: 50%; float: left">
        <img src="files/3dled.jpg" alt="3D LED kocka" style="width: 80%"><br>
    </div>


<?php include('layout/foot.php'); ?>